
  import 'package:flutter/material.dart';

class ProgressAnimation extends StatefulWidget {
  Animation<double> progressAnimation;
  AnimationController progressAnimcontroller;
  final ProgressCallback  setProgressAnim;

  ProgressAnimation({this.progressAnimation,this.progressAnimcontroller,this.setProgressAnim});

  @override
  _MyCompanyDetailsPage createState() => _MyCompanyDetailsPage();
  }

  class _MyCompanyDetailsPage extends State<ProgressAnimation>  with SingleTickerProviderStateMixin {

    AnimationController _progressAnimcontroller;
    final controller=PageController();
    @override
    void initState() {
      super.initState();

      _progressAnimcontroller = AnimationController(
        duration: Duration(milliseconds: 200),
        vsync: this,
      );

     widget.progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
          .animate(_progressAnimcontroller);

      _setProgressAnim(0, 1);
    }

    double growStepWidth, beginWidth, endWidth = 0.0;
    int totalPages = 6;

    _setProgressAnim(double maxWidth, int curPageIndex) {
      setState(() {
        growStepWidth = maxWidth / totalPages;
        beginWidth = growStepWidth * (curPageIndex - 1);
        endWidth = growStepWidth * curPageIndex;

        widget.progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
            .animate(_progressAnimcontroller);
      });

      _progressAnimcontroller.forward();
    }



    @override
    Widget build(BuildContext context) {
      return Scaffold();
      /* Animation<double> _progressAnimation;
  AnimationController _progressAnimcontroller;
  _progressAnimcontroller = AnimationController(
  duration: Duration(milliseconds: 200),
  vsync: this,
  );

  _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
      .animate(_progressAnimcontroller);

  _setProgressAnim(0, 1);
}

double growStepWidth, beginWidth, endWidth = 0.0;
int totalPages = 6;

_setProgressAnim(double maxWidth, int curPageIndex) {
  setState(() {
    growStepWidth = maxWidth / totalPages;
    beginWidth = growStepWidth * (curPageIndex - 1);
    endWidth = growStepWidth * curPageIndex;

    _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
        .animate(_progressAnimcontroller);
  });

  _progressAnimcontroller.forward();
}*/

    }
  }
  typedef ProgressCallback = void Function(double maxWidth, int curPageIndex);