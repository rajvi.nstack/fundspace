import 'package:flutter/material.dart';

/*class showDialogBox extends StatefulWidget {
 final ThemeData themeData;

  const showDialogBox({ Key key,this.themeData}) : super(key: key);

  @override
  _showDialogdState createState() => _showDialogdState(this.themeData);
}

class _showDialogdState extends State<showDialogBox> {
   ThemeData themeData;


   _showDialogdState(this.themeData);

   @override
  Widget build(BuildContext context) {
   themeData = Theme.of(context);

    return Scaffold(
      body: Container(child: _showDialog(context) ,),
    );
  }
    _showDialog(BuildContext context) {
   themeData = Theme.of(context);
    Dialog fancyDialog = Dialog(
      child: Container(
        height: 250.0,
        width: 280.0,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15),
              child: Container(
                width: double.infinity,
                height: 80,
                child: Column(
                  children: <Widget>[
                    Align(alignment: Alignment.center,
                        child: Text("Rhythm".toUpperCase(),
                          style: TextStyle(color: Colors.black,fontSize: 18),)),
                  ],
                ),
              ),
            ),
            Center(
              child: Container(
                width: double.infinity,
                height: 50,
                alignment: Alignment.bottomCenter,
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Are you sure you want to logout?",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                width: MediaQuery.of(context).size.width/2,
                child: Row(
                  children: <Widget>[
                    InkWell(
                      onTap: ()=>  Navigator.of(context).pop(),
                      child: Container(
                        //width: 140,
                        height: 70,
                        color: Color(0xFF1A237E),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "No",
                            style: widget.themeData.textTheme.button,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.of(context).pop();
                        //navigateToWelcomePage();
                      },
                      child: Container(
                        //width: 140,
                        height: 70,
                        color: Color(0xFFF5F5F5),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => fancyDialog);
  }
}*/

class DynamicDialog extends StatefulWidget {
  DynamicDialog(this.title,this.description,this.no,this.yes,this.data1,this.data2,{this.onSelect});

  final String title;
  final String description;
  final String no;
  final String yes;
  final String data1;
  final String data2;
 final Function onSelect;
  //final Function data2;


  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  String _title;
  String _description;
  String _data1;
  String _data2;
  String _no;
  String _yes;
  Function _onSelect;

  @override
  void initState() {
    _title = widget.title;
    _description=widget.description;
    _no=widget.no;
    _yes=widget.yes;
    _data1=widget.data1;
    _data2=widget.data2;
    _onSelect=widget.onSelect;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   ThemeData themeData = Theme.of(context);
   return Dialog(
     child:  Container(
       height: 300.0,
       width: 300.0,
       child: Stack(
         children: <Widget>[
           Padding(
             padding: const EdgeInsets.all(15),
             child: Container(
               width: double.infinity,
               height: 80,
               child: Column(
                 children: <Widget>[
                   Align(alignment: Alignment.center,
                       child: Text("Fundspace",
                         style: TextStyle(color: Colors.black,fontSize: 18),)),
                   /*Container(
                     width: double.infinity,
                     child: Align(alignment: Alignment.center,
                         child: Text(_data1,
                           style: TextStyle(color: Colors.black,fontSize: 18),)),

                   ),
                   Container(
                     width: double.infinity,
                     child: Align(alignment: Alignment.center,
                         child: Text(_data2,
                           style: TextStyle(color: Colors.black,fontSize: 18),)),
                   ),*/
                 ],
               ),
             ),
           ),
           Center(
             child: Container(
               width: double.infinity,
               height: 50,
               alignment: Alignment.bottomCenter,
               child: Align(
                 alignment: Alignment.center,
                 child: Text(
                   _description,
                   style: TextStyle(
                     color: Colors.black,
                     fontSize: 15,
                   ),
                 ),
               ),
             ),
           ),
           Align(
             alignment: Alignment.bottomLeft,
             child: Container(
               // width: MediaQuery.of(context).size.width/2,
               child: Row(
                 children: <Widget>[
                   InkWell(
                     onTap: ()=>  Navigator.of(context).pop(),
                     child: Container(
                       width: 150,
                       height: 70,
                       color: Colors.blue,
                       child: Align(
                         alignment: Alignment.center,
                         child: Text(
                           _no,
                           style: themeData.textTheme.button,
                         ),
                       ),
                     ),
                   ),
                   InkWell(
                     onTap: (){
                       _onSelect();
                      // Navigator.of(context).pop();
                       //navigateToWelcomePage();
                       /*Navigator.popUntil(context, (Route<dynamic> route){
                         bool shouldPop = false;
                         if(route.settings.name == HomePage.routeName){
                           shouldPop = true;
                         }
                         return shouldPop;
                       });*/
                     },
                     child: Container(
                       width: 150,
                       height: 70,
                       color: Color(0xFFF5F5F5),
                       child: Align(
                         alignment: Alignment.center,
                         child: Text(
                           _yes,
                           style: TextStyle(
                               color: Colors.black,
                               fontSize: 20,
                               fontWeight: FontWeight.w600),
                         ),
                       ),
                     ),
                   ),
                 ],
               ),
             ),
           )
         ],
       ),
     ),
   );
  }
}