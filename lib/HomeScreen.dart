import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/AddNewProjects/NewProjects.dart';
import 'package:fundspace/utils/Inbox/InboxPage.dart';
import 'package:fundspace/utils/Profile/ProfilePage.dart';
import 'package:fundspace/utils/ProjectPage.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _MyHomeScreen createState() => _MyHomeScreen();
}

class _MyHomeScreen extends State<HomeScreen> {
  bool _isLoading = false;
  int _selectedIndex = 0;


  void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
   List<Widget> _widgetOptions = <Widget>[
    Text("demo"),
     ProjectPage(),
    InboxPage(),
   ProfilePage()


  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
       leading: Image.asset("assets/images/FSicon.png"),
        title: Center(child: Text("Current Projects",
          style: TextStyle(color: Colors.black, fontFamily: 'Open Sans'),)),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ProgressWidget(
            isShow: _isLoading, opacity: 0.6, child: _widgetOptions.elementAt(_selectedIndex)),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        // new
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 24.0,
        // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            title: Text('NOTIFICATION'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('PROJECTS'),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.inbox),
              title: Text('INBOX')
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('PROFILE')
          ),
        ],
      ),
    );
  }


  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

}

