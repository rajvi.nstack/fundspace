import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/AssetLiabilities.dart';

class AssetLiabilitiesPages extends StatefulWidget {
  AssetLiabilitiesPages({Key key}) : super(key: key);

  @override
  _AssetLiabilitiesPagesState createState() => _AssetLiabilitiesPagesState();
}

class _AssetLiabilitiesPagesState extends State<AssetLiabilitiesPages> {
  bool _isLoading = false;
  ThemeData themeData;
  List<AssetLiabilities> assetsValue = List<AssetLiabilities>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setStreamData();
  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Scaffold(
        body: ProgressWidget(
            isShow: _isLoading, opacity: 0.6, child: streamBody()));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget streamBody() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          child: loginHeader(),
        ),
        Container(
          height: 400,

            child: ListView.separated(
                separatorBuilder: (context, index) => Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Divider(
                      color: Colors.blue,
                    )),
                padding: EdgeInsets.all(10),
                itemCount: assetsValue.length,
                itemBuilder: (context, index) {
                  return itemContent(index);
                })),
       Padding(
         padding: const EdgeInsets.all(20),
         child: loginButton(),
       )
      ],
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 38, top: 40, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Asset & Liabilities",
            style: TextStyle(fontSize: 28, fontFamily: 'Open Sans'),
          ),
          SizedBox(height: 20),
          Text(
            "The information below refers to the personal ",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "assets and liabilities of the borrower/company director",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),

        ],
      ),
    );
  }

  Widget itemContent(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("${assetsValue[index].assetsName}",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Open Sans',
                          fontSize: 16)),
                  Icon(Icons.keyboard_arrow_down,size: 30,color: Colors.black,)
                ],
              ),
              SizedBox(height: 10),
              Text("${assetsValue[index].totalValue}",
                  style: themeData.textTheme.subtitle.copyWith(
                      color: Colors.black87, fontFamily: 'Open Sans',)),
              SizedBox(height: 10),
              Text("${assetsValue[index].outStandingValue}",
                  style: themeData.textTheme.caption
                      .copyWith(color: Colors.black,fontFamily: 'Open Sans')),
            ],
          )
        ],
      ),
    );
  }
  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Complete this later",style: TextStyle(fontSize: 18,color: Colors.blue),),
              SizedBox(width: 10,),
            ],
          ),
        ),
        SizedBox(
          height: 48,
          width: 120,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Finish",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
             // _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void setStreamData() {
    AssetLiabilities assetLiabilities1 = AssetLiabilities();
    assetLiabilities1.assetsName = "Properties";
    assetLiabilities1.totalValue = "Total Market Value: £3,000,000";
    assetLiabilities1.outStandingValue = "Outstanding Mortgage Balance: £825,000";

    AssetLiabilities assetLiabilities2 = AssetLiabilities();
    assetLiabilities2.assetsName = "Loans, Current & Savings Account";
    assetLiabilities2.totalValue = "Total Balance: £26,000";
    assetLiabilities2.outStandingValue = "Total Loans/Overdraft: £12,500";

    AssetLiabilities assetLiabilities3 = AssetLiabilities();
    assetLiabilities3.assetsName = "Stocks, Shares & Bonds";
    assetLiabilities3.totalValue = "Total Value: £10,000";
    assetLiabilities3.outStandingValue = "Total Liabilities: £0";

    AssetLiabilities assetLiabilities4 = AssetLiabilities();
    assetLiabilities4.assetsName = "Other Assets";
    assetLiabilities4.totalValue = "Total Assets: £10,000";
    assetLiabilities4.outStandingValue = "Total Liabilities: £0";
    setState(() {
      assetsValue.add(assetLiabilities1);
      assetsValue.add(assetLiabilities2);
      assetsValue.add(assetLiabilities3);
      assetsValue.add(assetLiabilities4);
    });
  }

/* void navigateToStreamDetailsPage(StreamModel stream) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => StreamDetailsPage(
                streamModel: stream, homePageState: homePageState)));
  }

  void navigateToContactDetailsPage(StreamModel stream) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ContactDetailsPage(
                streamModel: stream, homePageState: homePageState)));
  }*/
}
