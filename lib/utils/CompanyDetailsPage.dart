import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class CompanyDetailsPage extends StatefulWidget {
  @override
  _MyCompanyDetailsPage createState() => _MyCompanyDetailsPage();
}

class _MyCompanyDetailsPage extends State<CompanyDetailsPage> {
  final FocusNode _companyName = FocusNode();

  final FocusNode _incorporationFocus = FocusNode();
  final FocusNode _companyRegistrationNumberFocus = FocusNode();
  final FocusNode _registeredAddressFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController companyNameController = new TextEditingController();

  TextEditingController incorporationController = new TextEditingController();
  TextEditingController companyRegistrationNumberController =
      new TextEditingController();
  TextEditingController registeredAddressController =
      new TextEditingController();

  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _company;
  String _incorporation;
  String _companyRegistrationNumber;
  String _registeredAddresse;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
    ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    loginHeader(),
                    SizedBox(height: 20),
                    loginContent(),
                    SizedBox(height: 15),
                    //separator(),
                    //socialLoginTabUI()
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 12),
              Text(
                "Company",
                style: TextStyle(color: Colors.black, fontSize: 18),
              ),
              SizedBox(height: 7),
              companyName(),
              SizedBox(height: 10),
              incorporation(),
              SizedBox(height: 10),
              companyRegistrationNumber(),
              SizedBox(height: 10),
              registeredAddress(),
              SizedBox(height: 55),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: loginButton(),
              ),
              SizedBox(height: 15),

              //newUser(),
            ],
          ),
        ),
      ),
    );
  }

  Widget companyName() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: companyNameController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _companyName,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _companyName, _incorporationFocus);
        },
        onSaved: (String val) {
          _company = val;
        },
        decoration: InputDecoration(
          hintText: "5 years",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Developer Experiance:",
        ));
  }

  Widget incorporation() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: incorporationController,
        validator: (String value) {
          return InputValidator.validateOccupation(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _incorporationFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(
              context, _incorporationFocus, _companyRegistrationNumberFocus);
        },
        onSaved: (String val) {
          _incorporation = val;
        },
        decoration: InputDecoration(
          hintText: "Property Developer",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Country of Incorporation:",
        ));
  }

  Widget companyRegistrationNumber() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: companyRegistrationNumberController,
        validator: (String value) {
          return InputValidator.validateEmploymentStatus(value);
        },
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        focusNode: _companyRegistrationNumberFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _companyRegistrationNumberFocus,
              _registeredAddressFocus);
        },
        onSaved: (String val) {
          _companyRegistrationNumber = val;
        },
        decoration: InputDecoration(
          hintText: "Employed",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Company Registration Number:",
        ));
  }

  Widget registeredAddress() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: registeredAddressController,
        validator: (String value) {
          return InputValidator.validateAnnualIncome(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _registeredAddressFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(
              context, _registeredAddressFocus, _registeredAddressFocus);
        },
        onSaved: (String val) {
          _registeredAddresse = val;
        },
        decoration: InputDecoration(
          hintText: "Address",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Registered Address:",
        ));
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_company");
      //print("LOGIN password : $_loaction");
      //navigateToProfilePage();
      // userLogin();

    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, bottom: 25, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () {
                return WillPopScope(onWillPop: () async {
                  return false;
                });
              },
              //onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Company Details",
            style: TextStyle(fontSize: 28, fontFamily: 'Open Sans'),
          ),
          SizedBox(height: 20),
          Text(
            "Once you’ve entered your basic profile information",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "below, you’ll be able to add more information about you",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "as a developer later. The more information you provide",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "the more likely you are to receive the most ompetitive",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "funding terms.",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
