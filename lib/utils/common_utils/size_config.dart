import 'package:flutter/cupertino.dart';

class   SizeConfig {
  static double _widthMultiplier;
  static double _heightMultiplier;
  static double screenWidth;
  static double screenHeight;
  static bool isTabSize = false;

  static void init(BoxConstraints boxConstraints, Orientation orientation) {
    screenWidth = boxConstraints.maxWidth;
    screenHeight = boxConstraints.maxHeight;

    if (screenWidth > 600) {
      isTabSize = true;
    } else {
      isTabSize = false;
    }

    if (orientation == Orientation.portrait) {
      _widthMultiplier = boxConstraints.maxWidth / 100;
      _heightMultiplier = boxConstraints.maxHeight / 100;
    } else {
      _widthMultiplier = boxConstraints.maxHeight / 100;
      _heightMultiplier = boxConstraints.maxWidth / 100;
    }
  }

  static double get(double size) {
    if (screenWidth > 600) {
      return size * _widthMultiplier*0.9;
    } else {
      return size * _widthMultiplier;
    }
  }

  static double getFontSize(double size) {
    if (screenWidth > 600) {
      return (size * 1.5);
    } else {
      return size;
    }
  }
}

