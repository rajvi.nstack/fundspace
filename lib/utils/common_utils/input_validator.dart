import 'package:fundspace/utils/constants/app_constants.dart';


mixin InputValidator {
  static String validateEmail(String value) {
    RegExp regex = new RegExp(AppConstants.EMAIL_PATTERN);
    if (value.isEmpty)
      return "Enter email address";
    else if (!regex.hasMatch(value))
      return "Enter valid email address";
    else
      return null;
  }

  static bool isValidateEmail(String value) {
    RegExp regex = new RegExp(AppConstants.EMAIL_PATTERN);
    if (value.isEmpty)
      return false;
    else if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  static String validatePassword(String value) {
    if (value.isEmpty)
      return "Enter password";
    else if (value.length < 6)
      return "Enter valid password";
    else
      return null;
  }

  static String validateFirstName(String value) {
    if (value.isEmpty)
      return "Enter first name";
    else if (value.length < 2)
      return "Enter valid first name";
    else
      return null;
  }
  static String validateComapnyName(String value) {
    if (value.isEmpty)
      return "Enter Comapany";
    else if (value.length < 2)
      return "Enter valid Company Name";
    else
      return null;
  }
  static String validateDeveloper(String value) {
    if (value.isEmpty)
      return "Enter Developer Experiance";
   /* else if (value.length < 0)
      return "Enter valid Developer Experiance";*/
    else
      return null;
  }
  static String validateResidential(String value) {
    if (value.isEmpty)
      return "Enter Residential Status";
    else if (value.length < 2)
      return "Enter valid Residential Status";
    else
      return null;
  }
  static String validateNationality(String value) {
    if (value.isEmpty)
      return "Enter Nationality";
    else if (value.length < 2)
      return "Enter valid Nationality";
    else
      return null;
  }
  static String validateAnnualIncome(String value) {
    if (value.isEmpty)
      return "Enter Annual Income";
    else if (value.length < 2)
      return "Enter valid Annual Income";
    else
      return null;
  }
  static String validateEmploymentStatus(String value) {
    if (value.isEmpty)
      return "Enter Employment Status";
    else if (value.length < 2)
      return "Enter valid Employment Status";
    else
      return null;
  }
  static String validateOccupation(String value) {
    if (value.isEmpty)
      return "Enter Occupation";
    else if (value.length < 2)
      return "Enter valid Occupation";
    else
      return null;
  }
  static String validateLocation(String value) {
    if (value.isEmpty)
      return "Enter LocationName";
    else if (value.length < 2)
      return "Enter valid Location Name";
    else
      return null;
  }


  static String validateLastName(String value) {
    if (value.isEmpty)
      return "Enter last name";
    else if (value.length < 2)
      return "Enter valid last name";
    else
      return null;
  }

  static String validateDateOfBirth(String value) {
    if (value.isEmpty)
      return "Enter date of birth";
    else if (value.length < 2)
      return "Enter valid date of birth";
    else
      return null;
  }
  /*static String validateCheckBox(bool value) {
    if (value==false)
      return "Select Terms & Conditions";
    else
      return null;
  }*/


  static String validateMobileNumber(String value) {
    Pattern pattern =
    '^(?:[+0]9)?[0-9]{10}';
    RegExp regex = new RegExp(pattern);

    if (value.isEmpty)
      return "Enter mobile number";
   else if (!regex.hasMatch(value))
    return 'Enter Valid Phone Number';
    else if (value.length < 10)
      return "Enter valid mobile number";
   /*else if (value.length != 10)
      return 'Mobile Number must be of 10 digit';*/

    else
      return null;
  }

  static bool isValidateMobileNumber(String value) {
    RegExp regex = new RegExp(AppConstants.MOBILE_PATTERN);
    value = value.replaceAll("[^a-zA-Z]+", "");
    value = value.replaceAll(" ", "");
    value = value.replaceAll("*", "");
    value = value.replaceAll("-", "");
    value = value.replaceAll("(", "");
    value = value.replaceAll(")", "");
    if (value.isEmpty)
      return false;
    else if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  /*static String validateFacebookUrl(String value) {
    RegExp regex = new RegExp(AppConstants.URL_PATTERN);
    if (value.isNotEmpty && !regex.hasMatch(value))
      return "Enter valid facebook profile url";
    else
      return null;
  }

  static String validateInstagramUrl(String value) {
    RegExp regex = new RegExp(AppConstants.URL_PATTERN);
    if (value.isNotEmpty && !regex.hasMatch(value))
      return "Enter valid instagram profile url";
    else
      return null;
  }

  static String validateTwitterUrl(String value) {
    RegExp regex = new RegExp(AppConstants.URL_PATTERN);
    if (value.isNotEmpty && !regex.hasMatch(value))
      return "Enter valid twitter profile url";
    else
      return null;
  }

  static String validateLinkedInUrl(String value) {
    RegExp regex = new RegExp(AppConstants.URL_PATTERN);
    if (value.isNotEmpty && !regex.hasMatch(value))
      return "Enter valid linkedin profile url";
    else
      return null;
  }

  static bool isValidateWebsiteUrl(String value) {
    RegExp regex = new RegExp(AppConstants.URL_PATTERN);
    if (value.isNotEmpty && !regex.hasMatch(value))
      return false;
    else
      return true;
  }*/

}
