import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/AllProjectModel.dart';
import 'package:fundspace/models/AssetLiabilities.dart';
import 'package:fundspace/utils/ProjectDetails.dart';
import 'package:fundspace/utils/ProjectPage.dart';

class AllProject extends StatefulWidget {
  AllProject({Key key}) : super(key: key);

  @override
  _AllProjectState createState() => _AllProjectState();
}

class _AllProjectState extends State<AllProject> {
  bool _isLoading = false;
  ThemeData themeData;
  List<AllProjectModel> projectData = List<AllProjectModel>();
  int _selectedIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setStreamData();
  }

  void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> _widgetOptions = <Widget>[
    Text("demo"),
    ProjectPage(),
    Text("demo1111"),
    Text("demo222222222")
  ];

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Image.asset("assets/images/FSicon.png"),
        title: Center(
            child: Text(
          "Current Projects",
          style: TextStyle(color: Colors.black, fontFamily: 'Open Sans'),
        )),
      ),
      body:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: streamBody()),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        // new
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 24.0,
        // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            title: Text('NOTIFICATION'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('PROJECTS'),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.inbox), title: Text('INBOX')),
          new BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('PROFILE')),
        ],
      ),
    );
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget streamBody() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 17),
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height/1.3,
            padding: EdgeInsets.only(bottom: 5),
            child: ListView.builder(
                //scrollDirection: Axis.vertical,
                shrinkWrap: true,
                padding: EdgeInsets.all(2),
                itemCount: projectData.length,
                itemBuilder: (BuildContext context, int index) {
                  return itemContent(index);
                }),
          ),
        ],
      ),
    );
  }

  Widget itemContent(int index) {
    return Padding(
      padding: const EdgeInsets.only(left:5.0,right: 15),
      child: InkWell(
        onTap: () => projectDetails(projectData[index]),
        child: Container(
          height: 150,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("${projectData[index].projectName}",
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Open Sans',
                              fontSize: 16)),
                      Container(
                        height: 30,
                        width: 20,
                        child: Text(
                          "${projectData[index].xValue}",
                          style: TextStyle(
                              backgroundColor: Colors.blue,
                              color: Colors.white),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: 100,
                        color: Colors.blue,
                        child: Center(
                          child: Text(
                            "${projectData[index].debt}",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text("${projectData[index].projectDec}",
                      style: themeData.textTheme.subtitle.copyWith(
                        color: Colors.black87,
                        fontFamily: 'Open Sans',
                      )),
                  SizedBox(height: 10),
                  Row(
                    children: <Widget>[
                      Text("${projectData[index].projectAmount}",
                          style: themeData.textTheme.caption.copyWith(
                              color: Colors.black, fontFamily: 'Open Sans')),
                      SizedBox(
                        width: 5,
                      ),
                      Text("${projectData[index].fundingRequired}",
                          style: themeData.textTheme.caption.copyWith(
                              color: Colors.black, fontFamily: 'Open Sans')),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void setStreamData() {
    AllProjectModel projectModel1 = AllProjectModel();
    projectModel1.projectName = "2 bedroom houses  ground up build";
    projectModel1.projectDec = "Barking, Essex";
    projectModel1.projectAmount = "£625,000";
    projectModel1.fundingRequired = "Total funding required";
    projectModel1.xValue = "x3";
    projectModel1.debt = "Senior Debt";

    AllProjectModel projectModel2 = AllProjectModel();
    projectModel2.projectName = "2 bedroom apartments commercial conversion";
    projectModel2.projectDec = "Canterbury, Kent";
    projectModel2.projectAmount = "£1,100,000";
    projectModel2.fundingRequired = "Total funding required";
    projectModel2.xValue = "x5";
    projectModel2.debt = "Bridging";

    AllProjectModel projectModel3 = AllProjectModel();
    projectModel3.projectName = "New build houses";
    projectModel3.projectDec = "Reading, Berkshire";
    projectModel3.projectAmount = "£725,000";
    projectModel3.fundingRequired = "Total funding required";
    projectModel3.xValue = "x4";
    projectModel3.debt = "Mezzanine";

    AllProjectModel projectModel4 = AllProjectModel();
    projectModel4.projectName = "2 bedroom houses ground up build";
    projectModel4.projectDec = "Barking, Essex";
    projectModel4.projectAmount = "£625,000";
    projectModel4.fundingRequired = "Total funding required";
    projectModel4.xValue = "x3";
    projectModel4.debt = "Senior Debt";
    setState(() {
      projectData.add(projectModel1);
      projectData.add(projectModel2);
      projectData.add(projectModel3);
      projectData.add(projectModel4);

    });
  }

  projectDetails(AllProjectModel model) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ProjectDetails(projectModel: model,)));
  }
}
