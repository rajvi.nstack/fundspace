import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:fundspace/custom_widget/DynamicDialog.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/AllProjectModel.dart';
import 'package:fundspace/utils/AddNewProjects/NewProjects.dart';
import 'package:fundspace/utils/ProjectPage.dart';
import 'package:fundspace/utils/common_utils/size_config.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ProjectDetails extends StatefulWidget {
  AllProjectModel projectModel;

  ProjectDetails({Key key, @required this.projectModel}) : super(key: key);

// ProjectDetails({Key key}) : super(key: key);

  @override
  _MyProjectDetails createState() => _MyProjectDetails();
}

class _MyProjectDetails extends State<ProjectDetails> {
  final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey1 = new GlobalKey<AnimatedCircularChartState>();
  AllProjectModel model;
  bool _isLoading = false;
  int _selectedIndex = 0;
  GoogleMapController mapController;
  final LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  /*void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  List<Widget> _widgetOptions = <Widget>[
    Text("demo"),
    ProjectPage(),
    Text("demo1111"),
    Text("demo222222222")


  ];*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue),
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
          "Project Overview",
          style: TextStyle(color: Colors.black, fontFamily: 'Open Sans'),
        )),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ProgressWidget(
            isShow: _isLoading, opacity: 0.6, child: loginBody()),
      ),
    );
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  googleMap(),
                  SizedBox(
                    height: 10,
                  ),
                  dateAdded(),
                  getData(),
                  SizedBox(
                    height: 20,
                  ),
                  lineBreak(),
                  SizedBox(
                    height: 20,
                  ),
                  priceValue(),
                  SizedBox(
                    height: 20,
                  ),
                  projectCost(),
                  SizedBox(
                    height: 20,
                  ),
                  allData(),
                  SizedBox(
                    height: 15,
                  ),
                  cardData(),
                  SizedBox(
                    height: 15,
                  ),
                  comparableProperties(),
                  SizedBox(
                    height: 15,
                  ),
                  uploadDocument(),
                  SizedBox(
                    height: 15,
                  ),
                  lineBreak(),
                  SizedBox(
                    height: 15,
                  ),
                  delete(),
                  SizedBox(
                    height: 15,
                  ),
                  /* loginHeader(),
                  loginContent(),
                  SizedBox(height: 15),
                  separator(),
                  socialLoginTabUI()*/
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget googleMap() {
    return Container(
      height: 230,
      child: GoogleMap(
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 11.0,
        ),
      ),
    );
  }

  Widget dateAdded() {
    return Container(
      color: Color(0xFFE3F2FD),
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Date Added:21/05/2019"),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    height: 40,
                    width: 150,
                    decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Center(child: Text("Development")),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text("Date Added:21/05/2019"),
            ),
          ],
        ),
      ),
    );
  }

  Widget getData() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Container(
          height: 180,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      "${widget.projectModel.projectAmount}",
                      style: TextStyle(fontSize: 25),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25, left: 5),
                    child: Text("${widget.projectModel.fundingRequired}"),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 20),
                child: Row(
                  children: <Widget>[
                    Text(
                      "${widget.projectModel.projectName}",
                      style: TextStyle(fontSize: 18),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Container(
                          color: Colors.blue,
                          height: 20,
                          width: 25,
                          child: Center(
                              child: Text(
                            "${widget.projectModel.xValue}",
                          ))),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget lineBreak() {
    return Container(
      height: 2,
      width: MediaQuery.of(context).size.width,
      color: Color(0xFFE0E0E0),
    );
  }

  Widget priceValue() {
    return Padding(
      padding: const EdgeInsets.only(left: 15,right: 10),
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width /3,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              height: 90,
              //width: MediaQuery.of(context).size.width/3,
              child: Card(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left:5,right: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                            "Current Value",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                        ),
                        Text(
                          "£800,00",
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                  )),
            ),
            Container(
              height: 90,
              //width: MediaQuery.of(context).size.width/3,
              child: Card(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left:5,right: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                            "Purchase Price",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                        ),
                        Text(
                          "£795,000",
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                  )),
            ),
            Container(
              height: 90,
              //width: MediaQuery.of(context).size.width/3,
              child: Card(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left:8,right: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                            "GDV",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                        ),
                        Text(
                          "£1,800,00",
                          style: TextStyle(fontSize: 12),
                        )
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget projectCost() {
    return Container(
      height: 432,
      width: MediaQuery.of(context).size.width / 2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 430,
            width: MediaQuery.of(context).size.width / 1.7,
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Card(
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 20, left: 20,bottom: 10),
                      child: Text(
                        "Project Costs",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    lineBreak(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0,top: 5),
                              child: projectData(),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          height: 320,
                          width: 2,
                          color: Color(0xFFE0E0E0),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: projectPrice(),
                            )
                          ],
                        ),
                      ],
                    ),
                    Container(
                      width: 270,
                      color: Colors.blue,
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(7.0)),
                        child: Text(
                          "Total: £615,450",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          Container(
            width: MediaQuery.of(context).size.width / 2.5,
            child: Column(
              children: <Widget>[
                AnimatedCircularChart(
                  key: _chartKey,
                  size: const Size(150.0, 150.0),
                  initialChartData: <CircularStackEntry>[
                    new CircularStackEntry(
                      <CircularSegmentEntry>[

                        new CircularSegmentEntry(
                          77,
                          Color(0xFFA5D6A7),
                          rankKey: 'remaining',
                        ),
                        new CircularSegmentEntry(
                          23,
                          Colors.blue[400],
                          rankKey: 'completed',
                        ),
                      ],
                      rankKey: 'progress',
                    ),
                  ],
                  chartType: CircularChartType.Radial,
                  percentageValues: true,
                  holeLabel: '23%',
                  labelStyle: new TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:12),
                  child: Text("Profit on Cost",style: TextStyle(fontSize: 15),),
                ),
                AnimatedCircularChart(
                  key: _chartKey1,
                  size: const Size(150.0, 150.0),
                  initialChartData: <CircularStackEntry>[
                    new CircularStackEntry(
                      <CircularSegmentEntry>[
                        new CircularSegmentEntry(
                          100,
                          Colors.blue[400],
                          rankKey: 'completed',
                        ),
                      ],
                      rankKey: 'progress',
                    ),
                  ],
                  chartType: CircularChartType.Radial,
                  percentageValues: true,
                 // holeRadius: 1,
                  holeLabel: '£384,550',
                  labelStyle: new TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:12),
                  child: Text("Developer Profit",style: TextStyle(fontSize: 15),),
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

  Widget projectData() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "- Stamp Duty",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Build Costs",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- VAT",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Legals",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 12),
          Text(
            "- Valuation",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- S106/CIL",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Professional Fees",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Warranties",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Surveyor",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Sales & Marketing",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "- Contingency",
            style: TextStyle(fontSize: 13),
          ),
        ]);
  }

  Widget projectPrice() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "£28,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£505,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£1,700",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£2,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 12),
          Text(
            "£1,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£60,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£5,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "TBC",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£750",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£2,000",
            style: TextStyle(fontSize: 13),
          ),
          SizedBox(height: 15),
          Text(
            "£10,000",
            style: TextStyle(fontSize: 13),
          ),
        ]);
  }

  Widget allData() {
    return Container(
      height: 280,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 20, left: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Accommodation",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Number of units: 2",
                          style: TextStyle(fontSize: 13),
                        ),
                        Text(
                          "Total bedrooms: 4",
                          style: TextStyle(fontSize: 13),
                        ),
                        Text(
                          "Total bedrooms: 2",
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Security",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "1st charge ",
                            style: TextStyle(fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Exit Strategy",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Sell ",
                            style: TextStyle(fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Term Required",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "6 Months ",
                            style: TextStyle(fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: 40,
                ),
                Container(
                  height: MediaQuery.of(context).size.width,
                  width: 2,
                  color: Color(0xFFE0E0E0),
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Dimensions",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Total sqft: 900",
                          style: TextStyle(fontSize: 13),
                        ),
                        Text(
                          "GDV per sqft: £225",
                          style: TextStyle(fontSize: 13),
                        ),
                        Text(
                          "Cost per sqft: £100 ",
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Planning",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Approved  ",
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Tenure",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Freehold ",
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Build Duration",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "6 Months ",
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget cardData() {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Container(
        width: MediaQuery.of(context).size.width/2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(

              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 100,

                    child: Card(
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Building Contractor",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Moran Construction Ltd \nRod Moran \nmoranconstruction.com",
                              style: TextStyle(fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Card(
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Architect",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Moran Construction Ltd \nRod Moran \nmoranconstruction.com",
                              style: TextStyle(fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 100,
                  child: Card(
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Solicitor",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Moran Construction Ltd \nRod Moran \nmoranconstruction.com",
                            style: TextStyle(fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 100,
                  child: Card(
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Structural Engineer",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Moran Construction Ltd \nRod Moran \nmoranconstruction.com",
                            style: TextStyle(fontSize: 13),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget comparableProperties() {
    return Container(
      height: 150,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 15),
                child: Text(
                  "Comparable Properties",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 15, top: 15),
                        child: Text(
                          "Data provided by:",
                          style: TextStyle(fontSize: 13),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          "rightmove.com ",
                          style: TextStyle(fontSize: 13),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          "and Zoopla.com",
                          style: TextStyle(fontSize: 13),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 100),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Image.asset(
                          "assets/images/rmsitelogo.png",
                          width: 100,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Image.asset(
                          "assets/images/zoopla.png",
                          width: 70,
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget uploadDocument() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SizedBox(
        height: 50,
        child: FlatButton(
          color: Colors.blue,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(7.0)),
          child: Text(
            "Upload  Supporting Documents",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {},
        ),
      ),
    );
  }

  Widget delete() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        height: 50,
        //decoration: BoxDecoration(border: Border.all(color: Colors.blue)),
        child: FlatButton(
          //color: Colors.blue,
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: Colors.blue, width: 1, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(7)),
          child: Text(
            "Delete",
            style: TextStyle(color: Colors.blue),
          ),
          onPressed: () {
            _showLogoutDialog();
          },
        ),
      ),
    );
  }
  void _showLogoutDialog() {

    setState(() {
      showDialog(
          context: context,
          builder: ((BuildContext context) {
            return DynamicDialog( 'RHYTHM', 'Are you sure you want to logout?', 'NO', 'YES','','',onSelect: (){},);
          }));
    });

  }
}
