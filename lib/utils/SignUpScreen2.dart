import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fundspace/utils/CreateProfile.dart';

import 'package:fundspace/utils/SplashScreen.dart';
import 'package:fundspace/utils/constants/icon_contants.dart';
import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

class SignUpScreen2 extends StatefulWidget {
  SignUpScreen2({Key key}) : super(key: key);

  @override
  _MySignUpScreen2 createState() => _MySignUpScreen2();
}

class _MySignUpScreen2 extends State<SignUpScreen2> {
  List<Slide> slides = new List();
  Function goToTab;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    slides.add(
      new Slide(
        title: "Navigate the marketplace",
        backgroundColor: Colors.white,
        styleTitle: TextStyle(
            color: Color(0xFF616161), fontSize: 23.0, fontFamily: 'Open Sans'),
        description:
            "Filter through multiple lenders and \n funding options and secure the \n most competitive terms.",
        styleDescription: TextStyle(
            color: Color(0xFF616161), fontSize: 16.0, fontFamily: 'Open Sans'),
        pathImage: "assets/images/Screen1.png",
        widthImage: 20,
        heightImage: 20,
      ),
    );
    slides.add(
      new Slide(
        title: "Get funding faster",
        styleTitle: TextStyle(
            color: Color(0xFF616161), fontSize: 23.0, fontFamily: 'Open Sans'),
        description:
            "Easily communicate and share \n information directly with \n underwriters and get faster \n decisions.",
        styleDescription: TextStyle(
            color: Color(0xFF616161), fontSize: 16.0, fontFamily: 'Open Sans'),
        pathImage: "assets/images/Screen2.png",
      ),
    );
    slides.add(
      new Slide(
          title: "Reduce upfront costs",
          styleTitle: TextStyle(
              color: Color(0xFF616161),
              fontSize: 23.0,
              fontFamily: 'Open Sans'),
          description: "Save up to 50% on lender \n  arrangement fees.",
          styleDescription: TextStyle(
              color: Color(0xFF616161),
              fontSize: 16.0,
              fontFamily: 'Open Sans'),
          pathImage: "assets/images/Screen3.png"),
    );
  }

  Widget renderDoneBtn() {
    return Container(
      color: Colors.blue,
      height: 35,
      width: 50,
      child: Center(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = new List();
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          margin: EdgeInsets.only(bottom: 100.0, top: 60.0),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 70,
              ),
              appLogo(),
              SizedBox(
                height: 80,
              ),
              GestureDetector(
                  child: Image.asset(
                currentSlide.pathImage,
                width: 120.0,
                height: 120.0,
                fit: BoxFit.contain,
              )),
              Container(
                child: Text(
                  currentSlide.title,
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
              Container(
                child: Text(
                  currentSlide.description,
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
            ],
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          new IntroSlider(
            slides: this.slides,
            listCustomTabs: this.renderListCustomTabs(),
            backgroundColorAllSlides: Colors.white,
            sizeDot: 13,
            colorDot: Colors.blue,

            colorActiveDot: Colors.blue,
            typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,

            renderDoneBtn: this.renderDoneBtn(),
            widthDoneBtn: 150,
            onDonePress: this.onDonePress,


            //colorDoneBtn: Color(0x33000000),
            //highlightColorDoneBtn: Color(0xff000000),
          ),
         /* SizedBox(
            height: 35,
            width: 70,
            child: Align(
              alignment: Alignment.bottomRight,
              child: FlatButton(
                color: Colors.blue,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7.0)),
                child: Text(
                  "Get Started",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => SignUpScreen2()));
                },
              ),
            ),
          )*/
          
        ],
      ),
    );
  }

  void onDonePress() {
    // Do what you want
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CreateProfile()),
    );
  }

  Widget appLogo() {
    return Image.asset(
      IconConstants.IC_APP_LOGO,
      width: 40,
      height: 40,
    );
  }
}

//demo:https://pub.dev/packages/intro_slider#-readme-tab-
