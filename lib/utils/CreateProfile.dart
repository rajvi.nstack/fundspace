import 'dart:async';
import 'dart:io';

import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/ProfilePages.dart';
import 'package:fundspace/utils/SignUpScreen.dart';
import 'package:fundspace/utils/SplashScreen.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

import 'package:fundspace/utils/constants/icon_contants.dart';
import 'package:image_picker/image_picker.dart';

class CreateProfile extends StatefulWidget {
  CreateProfile({Key key}) : super(key: key);

  @override
  _MyCreateProfileScreen createState() => _MyCreateProfileScreen();
}

class _MyCreateProfileScreen extends State<CreateProfile> {
  ThemeData themeData;
  bool _isLoading = false;
  final FocusNode _companyFocus = FocusNode();
  final FocusNode _locationFocus = FocusNode();
  final FocusNode _contactFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController companyController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();
  TextEditingController contactController = new TextEditingController();
  bool _autoValidate = false;
  bool passwordVisible=false;
  String _company;
  String _loaction;
  String _contact;
  File image;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    //loginNavigation(),
                    loginHeader(),
                    SizedBox(height: 20),
                    loginContent(),
                    SizedBox(height: 15),
                    //separator(),
                    //socialLoginTabUI()
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 10),
          imageInput(),
          SizedBox(height: 12),
          companyName(),
          SizedBox(height: 10),
          locationInput(),
          SizedBox(height: 10),
          contactNumber(),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: loginButton(),
          ),
          SizedBox(height: 15),

          //newUser(),
        ],
      ),
    );
  }
  Widget imageInput() {
    return new Container(
      width: 200.0,
      height: 200.0,
      child: new Stack(
        alignment: Alignment.center,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(15.0),
             decoration: new BoxDecoration(shape: BoxShape.circle,
                border: Border.all(color: Colors.blue,width: 2.0)
                 
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 50,right: 65),
            child: new Align(alignment: Alignment.bottomRight,
              child: InkWell(
                onTap: ()=>onClickImage(),
                child: new Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                      shape: BoxShape.circle
                  ),
                  alignment: Alignment.center,
                  child: new Icon(Icons.mode_edit,color: Colors.white,)
                ),
              ),),
          ),
           Container(
               height: 110,
               width: 110,
               color:Colors.white,
               child: image==null || image.path == null?Image.asset('assets/images/user.png',color: Colors.grey,):new Container(
                   width: 180.0,
                   height: 180.0,
                   decoration: new BoxDecoration(
                     shape: BoxShape.circle,

                     color: const Color(0xff7c94b6),
                     image: new DecorationImage(
                       image: new FileImage(
                           new File(image.path)),
                       fit: BoxFit.cover,
                     ),
                   ))),
        ],),

    );
   /* return Container(

      child: Center(
      child:  Container(
        height: 150,
        width: 180,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          border: new Border.all(
            width: 3.0,
            color: Colors.blue,

          ),
        ),
        child: ClipRRect(
         borderRadius: BorderRadius.circular(2.0),
          child: Image.asset(
            'assets/images/user.png',


          ),

        ),
      )
      )
    );*/

  }
 void onClickImage() {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) => CupertinoActionSheet(
          title: const Text('Choose Options'),
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            },
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: const Text('Camera'),
              onPressed: () {
                Navigator.pop(context, 'Camera');
                getImage(ImageSource.camera);
              },
            ),
            CupertinoActionSheetAction(
              child: const Text('Gallery'),
              onPressed: () {
                Navigator.pop(context, 'Gallery');
                getImage(ImageSource.gallery);
              },
            )
          ],
        ));
  }
  Future getImage(ImageSource imageSource) async {
    image = await ImagePicker.pickImage(source: imageSource);
    print(image);
    //if (image != null) processBusinessCardImage(image);
  }

  Widget companyName() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: companyController,
        validator: (String value) {
          return InputValidator.validateComapnyName(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _companyFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _companyFocus, _locationFocus);
        },
        onSaved: (String val) {
          _company = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your company name",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Company",));
  }

  Widget locationInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: locationController,
        validator: (String value) {
          return InputValidator.validateLocation(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _locationFocus,

        //obscureText: passwordVisible,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _locationFocus, _contactFocus);
        },
        onSaved: (String val) {
          _loaction = val;
        },
        decoration: InputDecoration(
          hintText: "Enter your location",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "Location",
          ));
  }
  Widget contactNumber() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: contactController,
        maxLength: 10,

        validator: (String value) {
          return InputValidator.validateMobileNumber(value);
        },
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        focusNode: _contactFocus,
       // obscureText: passwordVisible,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _companyFocus, _contactFocus);
        },
        onSaved: (String val) {
          _loaction = val;
        },
        decoration: InputDecoration(
          hintText: "Enter your Contact Number",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "Contact number",
        ));
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(Icons.navigate_next,color: Colors.white,)
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_company");
      print("LOGIN password : $_loaction");
      navigateToProfilePage();
      // userLogin();
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, bottom: 25, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: (){
                return WillPopScope(
                    onWillPop: () async {
                      return false;
                    });
              },
              //onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40,top: 80,right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 20,width: 20,),
          Text("Hii Ben,",style: TextStyle(fontSize: 28,fontFamily: 'Open Sans'),),
          SizedBox(height: 20),
          Text("Once you’ve entered your basic profile information \nbelow, you’ll be able to add more information about you \nas a "
              "developer later. The more information you provide \nthe more likely you are to receive the most "
              "ompetitive \nfunding terms.",style: TextStyle(fontSize: 13,fontFamily: 'Open Sans'),)
        ],
      ),
    );
  }

  

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void navigateToProfilePage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ProfilePages()));
  }


}
