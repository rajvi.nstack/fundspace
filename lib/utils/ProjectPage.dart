
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/AddNewProjects/NewProjects.dart';
import 'package:fundspace/utils/AllProject.dart';
import 'package:fundspace/utils/SignUpScreen.dart';
import 'package:fundspace/utils/SplashScreen.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';
import 'package:fundspace/utils/constants/icon_contants.dart';

class ProjectPage extends StatefulWidget {
  ProjectPage({Key key}) : super(key: key);

  @override
  _MyProjectPage createState() => _MyProjectPage();
}

class _MyProjectPage extends State<ProjectPage> {
  bool _isLoading = false;
  int _selectedIndex = 0;
  //int project=0;


  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ProgressWidget(
            isShow: _isLoading, opacity: 0.6, child: loginBody()),
      ),
    );
  }


  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget loginBody() {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Nothing to see here…", style: TextStyle(
                color: Colors.black, fontSize: 23, fontFamily: 'Open Sans'),),
            SizedBox(height: 10,),
            Text(
              "It looks like you have not added any new\nprojects yet. Just tap the button below to\nstart adding a new project you are looking to fund.",
              style: TextStyle(
                  color: Colors.black, fontSize: 13, fontFamily: 'Open Sans'),),
            SizedBox(height: 20,),
            loginButton()

          ],
        ),
      ),

    );
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 48,
          child: Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: FlatButton(
              color: Colors.blue,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Add New Project",
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                  )
                ],
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NewProjects()));
                // setLoading(true);
                // _validateInputs();
              },
            ),
          ),
        ),
      ],
    );
  }

  /*projectDetails() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => AllProject()));
  }*/


}

