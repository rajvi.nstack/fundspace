import 'dart:async';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/HomeScreen.dart';
import 'package:fundspace/utils/LoginScreen.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);


  @override
  _MySplashScreen createState() => _MySplashScreen();
}

class _MySplashScreen extends State<SplashScreen> with TickerProviderStateMixin{
  AnimationController controller;
  Animation<double> animation;

  startTime() async {
    var _duration = Duration(seconds: 3);
    return new Timer(_duration, loginPage);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller = AnimationController(
        duration: const Duration(seconds: 1), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    controller.forward();
    startTime();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: FadeTransition(
            opacity: animation,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  appLogo()
                ]
            )
        )
      ),

    );
  }


  Widget appLogo() {
    return Image.asset('assets/images/logo.png');
  }
  void loginPage()  async{

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen ()));

  }
}