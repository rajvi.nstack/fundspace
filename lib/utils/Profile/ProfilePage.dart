import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/AllProjectModel.dart';
import 'package:fundspace/models/StreamModel.dart';
import 'package:fundspace/utils/Inbox/ChatPage.dart';

class ProfilePage extends StatefulWidget {


  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ThemeData themeData;
  bool _isLoading = false;
  List<AllProjectModel> projectData = List<AllProjectModel>();
  ScrollController _rrectController = ScrollController();

  @override
  void dispose() {
    //_keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setStreamData();


  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Scaffold(
        body: SingleChildScrollView(
          child: ProgressWidget(
              isShow: _isLoading, opacity: 0.6, child: localBody()),
        ));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget localBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 20,),
        profileData(),
        SizedBox(height: 20,),
        bioData(),
        SizedBox(height: 20,),
        streamBody(),
        SizedBox(height: 20,),
        delete(),
        SizedBox(height: 20,),
        termsData(),
        SizedBox(height: 20,)
      ],
    );
  }
  Widget profileData(){
    return Padding(
      padding: const EdgeInsets.only(left:20,right: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 230,
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all( Radius.circular(13.0)),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(height: 15,),
              CircleAvatar(
                backgroundImage: ExactAssetImage('assets/images/Sundarbhai.png'),
                minRadius: 60,
                maxRadius: 60,
                backgroundColor: Colors.white,
              ),
              SizedBox(height: 10,),
              Text("Richard Smith",style: TextStyle(fontSize: 20),),
              Text("Priory properties Ltd",style: TextStyle(fontSize: 15),),
              Text("London, United Kingdom",style: TextStyle(fontSize: 15),)
            ],
          ),
        ),
      ),
    );
  }
  Widget bioData(){
    return Padding(
      padding: const EdgeInsets.only(left:20,right: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 230,
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all( Radius.circular(13.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 15,),
              Padding(
                padding: const EdgeInsets.only(left:20),
                child: Text("Lender Bio",style: TextStyle(fontSize: 20),),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget streamBody() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 17),
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height/1.4,
            padding: EdgeInsets.only(bottom: 5),
            child: DraggableScrollbar.rrect(
              controller: _rrectController,
              //backgroundColor: Colors.blue,

              child: ListView.builder(
                  controller: _rrectController,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  padding: EdgeInsets.all(2),
                  itemCount: projectData.length,
                  itemBuilder: (BuildContext context, int index) {
                    return itemContent(index);
                  }),
            ),
          ),
        ],
      ),
    );
  }

  Widget itemContent(int index) {
    return Padding(
      padding: const EdgeInsets.only(left:20,right: 20),
      child: Container(
        height: 150,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("${projectData[index].projectName}",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Open Sans',
                            fontSize: 16)),
                    Container(
                      height: 30,
                      width: 20,
                      child: Text(
                        "${projectData[index].xValue}",
                        style: TextStyle(
                            backgroundColor: Colors.blue,
                            color: Colors.white),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 100,
                      color: Colors.blue,
                      child: Center(
                        child: Text(
                          "${projectData[index].debt}",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                Text("${projectData[index].projectDec}",
                    style: themeData.textTheme.subtitle.copyWith(
                      color: Colors.black87,
                      fontFamily: 'Open Sans',
                    )),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Text("${projectData[index].projectAmount}",
                        style: themeData.textTheme.caption.copyWith(
                            color: Colors.black, fontFamily: 'Open Sans')),
                    SizedBox(
                      width: 5,
                    ),
                    Text("${projectData[index].fundingRequired}",
                        style: themeData.textTheme.caption.copyWith(
                            color: Colors.black, fontFamily: 'Open Sans')),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void setStreamData() {
    AllProjectModel projectModel1 = AllProjectModel();
    projectModel1.projectName = "2 bedroom houses  ground up build";
    projectModel1.projectDec = "Barking, Essex";
    projectModel1.projectAmount = "£625,000";
    projectModel1.fundingRequired = "Total funding required";
    projectModel1.xValue = "x3";
    projectModel1.debt = "Senior Debt";

    AllProjectModel projectModel2 = AllProjectModel();
    projectModel2.projectName = "2 bedroom apartments commercial conversion";
    projectModel2.projectDec = "Canterbury, Kent";
    projectModel2.projectAmount = "£1,100,000";
    projectModel2.fundingRequired = "Total funding required";
    projectModel2.xValue = "x5";
    projectModel2.debt = "Bridging";

    AllProjectModel projectModel3 = AllProjectModel();
    projectModel3.projectName = "New build houses";
    projectModel3.projectDec = "Reading, Berkshire";
    projectModel3.projectAmount = "£725,000";
    projectModel3.fundingRequired = "Total funding required";
    projectModel3.xValue = "x4";
    projectModel3.debt = "Mezzanine";

    AllProjectModel projectModel4 = AllProjectModel();
    projectModel4.projectName = "2 bedroom houses ground up build";
    projectModel4.projectDec = "Barking, Essex";
    projectModel4.projectAmount = "£625,000";
    projectModel4.fundingRequired = "Total funding required";
    projectModel4.xValue = "x3";
    projectModel4.debt = "Senior Debt";
    setState(() {
      projectData.add(projectModel1);
      projectData.add(projectModel2);
      projectData.add(projectModel3);
      projectData.add(projectModel4);

    });
  }

  Widget delete() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        //decoration: BoxDecoration(border: Border.all(color: Colors.blue)),
        child: FlatButton(
          //color: Colors.blue,
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: Colors.blue, width: 1, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(7)),
          child: Text(
            "Delete",
            style: TextStyle(color: Colors.blue),
          ),
          onPressed: () {
           print("demo");
          },
        ),
      ),
    );
  }

  Widget termsData(){
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Terms of Use",style: TextStyle(color: Colors.blue,fontSize: 18),),
            Text("Privacy Policy",style: TextStyle(color: Colors.blue,decoration: TextDecoration.underline,fontSize: 18)),
          ],
        ),
      ),
    );
  }



}