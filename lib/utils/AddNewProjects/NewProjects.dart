import 'package:flutter/material.dart';
import 'package:fundspace/utils/AddNewProjects/AddProject1.dart';
import 'package:fundspace/utils/AddNewProjects/AddProject2.dart';
import 'package:fundspace/utils/AddNewProjects/AddProject3.dart';
import 'package:fundspace/utils/AddNewProjects/AddProject4.dart';
import 'package:fundspace/utils/AddNewProjects/AddProject6.dart';
import 'package:fundspace/utils/AddNewProjects/Addproject5.dart';
import 'package:fundspace/utils/AddNewProjects/Addproject7.dart';

class NewProjects extends StatefulWidget {
  NewProjects({Key key}) : super(key: key);

  @override
  _MyNewProjects createState() => _MyNewProjects();
}

class _MyNewProjects extends State<NewProjects>  with SingleTickerProviderStateMixin{
  ThemeData themeData;
  bool _isLoading = false;
  Animation<double> _progressAnimation;
  AnimationController _progressAnimcontroller;
  final controller=PageController();

  @override
  void initState() {
    super.initState();

    _progressAnimcontroller = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
    );

    _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
        .animate(_progressAnimcontroller);

    _setProgressAnim(0, 1);
  }

  double growStepWidth, beginWidth, endWidth = 0.0;
  int totalPages = 8;

  _setProgressAnim(double maxWidth, int curPageIndex) {
    setState(() {
      growStepWidth = maxWidth / totalPages;
      beginWidth = growStepWidth * (curPageIndex - 1);
      endWidth = growStepWidth * curPageIndex;

      _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
          .animate(_progressAnimcontroller);
    });

    _progressAnimcontroller.forward();
  }
  int step=1;
  int _selectedIndex = 1;


  void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }



  @override
  Widget build(BuildContext context) {
    var mediaQD = MediaQuery.of(context);
    var maxWidth = mediaQD.size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: Icon(Icons.keyboard_arrow_left,color: Colors.black,),
          title: Center(
            child: Text(
              "Add Project",
              style: TextStyle(color: Colors.black),
            ),
          ),

        ),
        body:Column(
          children: <Widget>[

            Container(
              color: Colors.transparent,
              child: Row(
                children: <Widget>[
                  AnimatedProgressBar(
                    animation: _progressAnimation,
                  ),
                  Expanded(
                    child: Container(
                      height: 2.0,
                      width: double.infinity,
                      decoration: BoxDecoration(color: Color(0xFFEEEEEE)),
                    ),
                  )
                ],
              ),
            ),
            Flexible(
              child: PageView(
                controller:controller ,
                scrollDirection: Axis.horizontal,
                physics: NeverScrollableScrollPhysics(),
                onPageChanged: (i) {
                  //index i starts from 0!
                  setState(() {
                    _progressAnimcontroller.reset(); //reset the animation first
                    _setProgressAnim(maxWidth, i + 1);
                  });

                },

                children: <Widget>[
                  AddProjects1(),
                  AddProjects2(),
                  AddProjects3(),
                  AddProjects4(),
                  AddProjects5(),
                  AddProjects6(),
                  AddProjects7(),
                  //AddProjects6(),
                  //aboutYouPage(),
                  /*AboutYouPage(),
                  CompanyDetailsPage(),
                  CreditPositionPage(),
                  AssetLiabilitiesPages(),
                  ProjectScreen()*/
                ],
              ),
            ),
          ],
        ),

    );
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }
}

class AnimatedProgressBar extends AnimatedWidget {
  AnimatedProgressBar({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Container(
      height: 4.0,
      width: animation.value,
      decoration: BoxDecoration(color: Colors.blue),
    );
  }
}