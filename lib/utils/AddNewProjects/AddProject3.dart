import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects3 extends StatefulWidget {
  @override
  _MyAddProjects3 createState() => _MyAddProjects3();
}

class _MyAddProjects3 extends State<AddProjects3> {
  final FocusNode _streetAddressFocus = FocusNode();
  final FocusNode _cityFocus = FocusNode();
  final FocusNode _countryPermissionFocus = FocusNode();
  final FocusNode _postalCodeFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController streetAddressController = new TextEditingController();
  TextEditingController cityController = new TextEditingController();
  TextEditingController countryController = new TextEditingController();
  TextEditingController postalCodeController = new TextEditingController();

  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _streetAddress;
  String _city;
  String _country;
  String _postalCode;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            streetAddress(),
            SizedBox(height: 20),
            cityData(),
            SizedBox(height: 20,),
            countryData(),
            SizedBox(height: 20,),
            postalCode(),
            SizedBox(height: 40,),
            SizedBox(height: 40,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }


  Widget streetAddress() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: streetAddressController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _streetAddressFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _streetAddressFocus,_cityFocus);
        },
        onSaved: (String val) {
          _streetAddress = val;
        },
        decoration: InputDecoration(
          hintText: "5 London Rd SW10 4TT",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "Street address",

        ));
  }
  Widget cityData() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: cityController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _cityFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _cityFocus, _countryPermissionFocus);
        },
        onSaved: (String val) {
          _city = val;
        },
        decoration: InputDecoration(
          hintText: "e.g 4 Palace road",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "City",

        ));
  }
  Widget countryData() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: countryController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _countryPermissionFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _countryPermissionFocus, _postalCodeFocus);
        },
        onSaved: (String val) {
          _country = val;
        },
        decoration: InputDecoration(
          hintText: "e.g 4 Palace road",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "Country",

        ));
  }
  Widget postalCode() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: postalCodeController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _postalCodeFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _postalCodeFocus, _postalCodeFocus);
        },
        onSaved: (String val) {
          _postalCode = val;
        },
        decoration: InputDecoration(
          hintText: "SW6 6PU",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
          labelText: "Postal Code",

        ));
  }
  Widget linView(){
    return Container(
      width: double.infinity,
      height: 3,
      color: Colors.grey,
    );
  }




  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_streetAddress");
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "Do you have full planning\npermission?",
          style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }


  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Where’s your project  \nlocated?",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus,FocusNode nextFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}