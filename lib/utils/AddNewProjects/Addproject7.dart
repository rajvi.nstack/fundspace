import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/AddNewProjects/CompleteProject.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects7 extends StatefulWidget {
  @override
  _MyAddProjects7 createState() => _MyAddProjects7();
}

class _MyAddProjects7 extends State<AddProjects7> {
  final FocusNode _contributeFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController contributeController = new TextEditingController();
  int  selectRadio;



  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;

  String _contribute;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectRadio=0;
  }
  setSelectRadio(int val){
    setState(() {
      selectRadio=val;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            security(),
            SizedBox(height: 20),
            linView(),
            SizedBox(height: 50),
            loginFooter(),
            SizedBox(height: 20,),
            contribute(),
            SizedBox(height: 50,),

            SizedBox(height: 40,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }


  Widget security() {
    return Row(
      children: <Widget>[
        Radio(value: 1, groupValue: selectRadio,
            activeColor: Colors.blue,

            onChanged: (val){
              print(val);
              setSelectRadio(val);
            }),
        Text("Sell ",style: TextStyle(fontSize: 18),),
        Padding(
          padding: const EdgeInsets.only(left:50.0),
          child: Radio(value: 2, groupValue: selectRadio,
              activeColor: Colors.blue,
              onChanged: (val){
                print(val);
                setSelectRadio(val);
              }),
        ),
        Text("Refinance",style: TextStyle(fontSize: 18),),

      ],
    );
  }

  Widget contribute() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: contributeController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _contributeFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _contributeFocus);
        },
        onSaved: (String val) {
          _contribute = val;
        },
        decoration: InputDecoration(
          hintText: "e.g. £100,000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),

        ));
  }

  Widget linView(){
    return Container(
      width: double.infinity,
      height: 3,
      color: Color(0xFFD6D6D6),
    );
  }




  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Share",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CompleteProject()));
              //_validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_contribute");

    } else {
      //If all data are not valid then start auto validation.
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CompleteProject()));
      setState(() {
        _autoValidate = true;
      });
    }
  }
  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "How much capital do you have to \ncontribute to the project?",
          style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }


  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "What’s your exit \n strategy?",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}