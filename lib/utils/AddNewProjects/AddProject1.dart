import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/ProgressAmination.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects1 extends StatefulWidget {
/*  @override
  _MyAddProjects1 createState() => _MyAddProjects1();*/
  @override
  State<StatefulWidget> createState() {
    return _MyAddProjects1();
  }

}

class _MyAddProjects1 extends State<AddProjects1> {
  final FocusNode _apartmentsFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController apartmentsController = new TextEditingController();
  Animation<double> _progressAnimation;
  AnimationController _progressAnimcontroller;


  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _apartments;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),

            SizedBox(height: 7),
            apartments(),
            SizedBox(height: 10),

            SizedBox(height: 260),
            linView(),
            //SizedBox(height: 10,),
            loginFooter(),
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }

  Widget apartments() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: apartmentsController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _apartmentsFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _apartmentsFocus);
        },
        onSaved: (String val) {
          _apartments = val;
        },
        decoration: InputDecoration(
          hintText: "e.g 2,2 bedroom apartments",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),

        ));
  }
  Widget linView(){
    return Container(
      width: double.infinity,
      height: 3,
      color: Colors.grey,
    );
  }




  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_apartments");
      //ProgressAnimation(progressAnimation: _progressAnimation,progressAnimcontroller: _progressAnimcontroller,setProgressAnim: (){},);
       //reset the animation first
     // _setProgressAnim(maxWidth, i + 1);
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "When you’re adding your project information, rest assured all of the details entered are kept confidential"
              "and are only visible to potential funding providers",
          style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }


  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Create a title for your project\nor funding requirement",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}