import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects6 extends StatefulWidget {
  @override
  _MyAddProjects6 createState() => _MyAddProjects6();
}

class _MyAddProjects6 extends State<AddProjects6> {
  final FocusNode _longFundingFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController longFundingController = new TextEditingController();
  int  selectRadio;



  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;

  String _longFunding;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectRadio=0;
  }
  setSelectRadio(int val){
    setState(() {
      selectRadio=val;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            security(),
            SizedBox(height: 20),
            linView(),
            SizedBox(height: 50),
            loginFooter(),
            SizedBox(height: 20,),
            longFunding(),
            SizedBox(height: 50,),

            SizedBox(height: 40,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }


  Widget security() {
    return Container(
      width: MediaQuery.of(context).size.width/2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,

        children: <Widget>[
          Radio(value: 1, groupValue: selectRadio,
              activeColor: Colors.blue,
              onChanged: (val){
                  print(val);
                  setSelectRadio(val);
              }),
          Text("1st charge",style: TextStyle(fontSize: 15),),
          Padding(
            padding: const EdgeInsets.only(left:10.0),
            child: Radio(value: 2, groupValue: selectRadio,
                activeColor: Colors.blue,
                onChanged: (val){
                  print(val);
                  setSelectRadio(val);
                }),
          ),
          Text("2nd charge",style: TextStyle(fontSize: 15),),

        ],
      ),
    );
  }

  Widget longFunding() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: longFundingController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _longFundingFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _longFundingFocus);
        },
        onSaved: (String val) {
          _longFunding = val;
        },
        decoration: InputDecoration(
          hintText: "3 months",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15,color: Colors.blue),

        ));
  }

  Widget linView(){
    return Container(
      width: double.infinity,
      height: 3,
      color: Color(0xFFD6D6D6),
    );
  }




  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_longFunding");
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "How long do you need the \nfunding for?",
          style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }


  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "What type of security are you \noffering?",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}