import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects5 extends StatefulWidget {
  @override
  _MyAddProjects5 createState() => _MyAddProjects5();
}

class _MyAddProjects5 extends State<AddProjects5> {
  final FocusNode _currentValueFocus = FocusNode();
  final FocusNode _projectGDVFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController currentValueController = new TextEditingController();
  TextEditingController projectGDVController = new TextEditingController();


  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _currentValue;
  String _projectGDV;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            currentValue(),
            SizedBox(height: 50),
            loginFooter(),
            SizedBox(height: 20,),
            projectGDV(),
            SizedBox(height: 50,),

            SizedBox(height: 40,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }


  Widget currentValue() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: currentValueController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _currentValueFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _currentValueFocus);
        },
        onSaved: (String val) {
          _currentValue = val;
        },
        decoration: InputDecoration(
          hintText: "£654,000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),

        ));
  }

  Widget projectGDV() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: projectGDVController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _projectGDVFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _projectGDVFocus);
        },
        onSaved: (String val) {
          _projectGDV = val;
        },
        decoration: InputDecoration(
          hintText: "£635,000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),

        ));
  }

  Widget linView(){
    return Container(
      width: double.infinity,
      height: 3,
      color: Colors.grey,
    );
  }




  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_projectGDV");
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "What’s your projects GDV?",
          style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }


  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "What’s the current value of \nthe site?",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}