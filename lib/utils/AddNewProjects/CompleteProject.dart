
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/AllProject.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class CompleteProject extends StatefulWidget {
  @override
  _MyCompleteProject createState() => _MyCompleteProject();
}

class _MyCompleteProject extends State<CompleteProject> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child:loginBody()
        ));
  }
  Widget loginBody(){
    return Padding(
      padding: const EdgeInsets.only( top:50,right: 20,left: 20,bottom: 30),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        //color: Colors.grey,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(25),border: Border.all(color: Colors.grey,width: 2)),
        //decoration: BoxDecoration(border: Border.all(width: 3)),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top:80.0,right: 10,left: 10),
              child: Text("Your Project has been \nsuccessfully posted!",style: TextStyle(fontSize: 23,color: Colors.blue),),
            ),
            Padding(
              padding: const EdgeInsets.only(top:100),
              child: Center(child: InkWell(
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AllProject()));
                  },
                  child: Image.asset("assets/images/right.png",width: 120,))),
            ),
            Padding(
              padding: const EdgeInsets.only(top:130,left: 8,right: 8),
              child: Text("You can now add further information to your project Overview from the Projects tab below.",style: TextStyle(fontSize: 16,color: Colors.grey),),
            ),
          ],
        ),
      ),
    );
  }


}
