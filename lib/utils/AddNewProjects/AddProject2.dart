import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AddProjects2 extends StatefulWidget {
  @override
  _MyAddProjects2 createState() => _MyAddProjects2();
}

class _MyAddProjects2 extends State<AddProjects2> {
  final FocusNode _fundingTypeFocus = FocusNode();
  final FocusNode _planningPermissionFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController fundingTypeController = new TextEditingController();
  TextEditingController planningPermissionController =
      new TextEditingController();

  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _fundingType;
  String _planningPermission;
  List<String> data = [
    "demo",
    "demo",
    "demo",
    "demo",
    "demo",
    "demo",
    "demo",
    "demo",
    "demo",
    "demo"
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
    ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  loginContent(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            fundingType(),
            SizedBox(height: 20),
            //SizedBox(height: 10,),
            /*addData(),
            SizedBox(
              height: 10,
            ),*/
            loginFooter(),
            SizedBox(
              height: 10,
            ),
            planningPermission(),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }


  Widget fundingType() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: fundingTypeController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _fundingTypeFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _fundingTypeFocus);
        },
        onSaved: (String val) {
          _fundingType = val;
        },
        onTap: (){
          final act=CupertinoActionSheet(

            actions: <Widget>[
              CupertinoActionSheetAction(
                child: Text("Demo1"),
                onPressed: () => print("Demo"),
              ),
              CupertinoActionSheetAction(
                child: Text("Demo2"),
                onPressed: () => print("Demo"),
              ),
              CupertinoActionSheetAction(
                child: Text("Demo3"),
                onPressed: () => print("Demo"),
              ),
              CupertinoActionSheetAction(
                child: Text("Demo4"),
                onPressed: () => print("Demo"),
              ),
              CupertinoActionSheetAction(
                child: Text("Demo5"),
                onPressed: () => print("Demo"),
              ),
              CupertinoActionSheetAction(
                child: Text("Demo6"),
                onPressed: () => print("Demo"),
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          );
          showCupertinoModalPopup(context: context, builder: (BuildContext context)=>act);
        },
        decoration: InputDecoration(
          hintText: "Development",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
        ));
  }

  Widget planningPermission() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: planningPermissionController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _planningPermissionFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _planningPermissionFocus);
        },
        onSaved: (String val) {
          _planningPermission = val;
        },
        decoration: InputDecoration(
          hintText: "Yes",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
        ));
  }

  Widget linView() {
    return Container(
      width: double.infinity,
      height: 3,
      color: Colors.grey,
    );
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_fundingType");
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginFooter() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
          width: 20,
        ),
        Text(
          "Do you have full planning\npermission?",
          style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
        ),
        SizedBox(height: 20),
      ],
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "What type of funding do you \nneed?",
            style: TextStyle(fontSize: 20, fontFamily: 'Open Sans'),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus) {
    currentFocus.unfocus();
    //FocusScope.of(context).requestFocus(nextFocus);
  }
}
