import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/SignUpScreen2.dart';
import 'package:fundspace/utils/SplashScreen.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';
import 'package:fundspace/utils/common_utils/size_config.dart';
import 'package:fundspace/utils/constants/icon_contants.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key}) : super(key: key);

  @override
  _MySignUpScreen createState() => _MySignUpScreen();
}

class _MySignUpScreen extends State<SignUpScreen> {
  ThemeData themeData;
  bool _isLoading = false;
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  static final GlobalKey<ScaffoldState> scaffoldKey =
      new GlobalKey<ScaffoldState>();
  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _autoValidate = false;
  String _email;
  String _password;
  String _firstName;
  String _lastName;
  bool isChecked = false;
  bool isChecked1 = false;

   bool passwordVisible=true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ProgressWidget(
              isShow: _isLoading, opacity: 0.6, child: loginBody()),
        ));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    loginNavigation(),
                    loginHeader(),
                    loginContent(),
                    SizedBox(height: 15),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 10),
          firstNameInput(),
          SizedBox(height: 10),
          lastNameInput(),
          SizedBox(height: 12),
          emailInput(),
          SizedBox(height: 10),
          passwordInput(),
          SizedBox(height: 30),
          termsCondition(),
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: loginButton(),
          ),
          SizedBox(height: 15),

          //newUser(),
        ],
      ),
    );
  }

  Widget termsCondition() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Checkbox(
              value: isChecked,
              onChanged: (value) {
                setState(() {
                  isChecked=value;
                });
              },
            ),
            Text(
              "I agree to Fundspace Terms and \nService and Privacy Police ",
              style: TextStyle(color: Colors.grey, fontSize: 15),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Checkbox(
              value: isChecked1,
              onChanged: (value1) {
                setState(() {
                  isChecked1 = value1;
                });
              },
            ),
            Text(
              "I agree to receive news and product \n updates  from Fundspace ",
              style: TextStyle(color: Colors.grey, fontSize: 15),
            ),
          ],
        ),
      ],
    );
  }

  buildSnackbar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              message,
              style: TextStyle(color: Colors.white),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
      backgroundColor: Colors.black,
    ));
  }

  Widget firstNameInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: firstNameController,
        validator: (String value) {
          return InputValidator.validateFirstName(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _firstNameFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
        },
        onSaved: (String val) {
          _firstName = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your Firstname",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "First Name"));
  }

  Widget lastNameInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: lastNameController,
        validator: (String value) {
          return InputValidator.validateLastName(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _lastNameFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _lastNameFocus, _emailFocus);
        },
        onSaved: (String val) {
          _lastName = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your lastname",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Last Name"));
  }

  Widget emailInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: emailController,
        validator: (String value) {
          return InputValidator.validateEmail(value);
        },
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        focusNode: _emailFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _emailFocus, _passwordFocus);
        },
        onSaved: (String val) {
          _email = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your email",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Email",
            suffixIcon: Icon(Icons.done)));
  }

  Widget passwordInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: passwordController,

        validator: (String value) {
          return InputValidator.validatePassword(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _passwordFocus,
        obscureText: passwordVisible,
        onFieldSubmitted: (term) {
          _validateInputs();
        },
        onSaved: (String val) {
          _password = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your password",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Password",
          suffixIcon: GestureDetector(
            dragStartBehavior: DragStartBehavior.down,
            onTap: () {
              setState(() {
                passwordVisible = !passwordVisible;
              });
            },
            child: Icon(
              passwordVisible ? Icons.visibility_off : Icons.visibility,
              //semanticLabel: _obscureText ? 'show password' : 'hide password',
            ),
          ),));
  }

  Widget loginButton() {
    return SizedBox(
      height: 45,
      child: FlatButton(
        color: Colors.blue,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(7.0)),
        child: Text(
          "Sign Up",
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () {
          /*if (isChecked == false) {
            setState(() {
              isChecked = true;
            });
          } else if (isChecked == true) {
            setState(() {
              buildSnackbar("Select");
              isChecked = false;
            });
          }*/
          /* setState(() {
            if(isChecked==false){
              buildSnackbar("Please Select Terms and Conditions");
            }
          });*/

          //_validateInputs();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SignUpScreen2()));
        },
      ),
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      print("LOGIN email : $_email");
      print("LOGIN password : $_password");
      //setLoading(true);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => SignUpScreen2()));
      //navigateToHomePage();
      // userLogin();
    } else {
      //If all data are not valid then start auto validation.
      // setLoading(false);
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        appLogo(),
        SizedBox(height: 10),
      ],
    );
  }

  Widget appLogo() {
    return Image.asset(
      IconConstants.IC_APP_LOGO,
      width: 200,
      height: 200,
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void navigateToForgotPasswordPage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SplashScreen()));
  }
}
