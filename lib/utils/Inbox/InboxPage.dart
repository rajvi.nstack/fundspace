import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/StreamModel.dart';
import 'package:fundspace/utils/Inbox/ChatPage.dart';

class InboxPage extends StatefulWidget {


  InboxPage({Key key}) : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<InboxPage> {
  ThemeData themeData;
  bool _isLoading = false;
  List<InboxModel> streamList = List<InboxModel>();



  //KeyboardVisibilityNotification _keyboardVisibility = new KeyboardVisibilityNotification();
  int _keyboardVisibilitySubscriberId;
  bool _keyboardState;

  @override
  void dispose() {
   //_keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setStreamData();

    /*_keyboardState = _keyboardVisibility.isKeyboardVisible;

    _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
      onChange: (bool visible) {
        setState(() {
          _keyboardState = visible;
        });
      },
    );*/
  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Scaffold(
        body: ProgressWidget(
            isShow: _isLoading, opacity: 0.6, child: notificationBody()));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget notificationBody() {
    return Column(
      children: <Widget>[
        Expanded(
            child: ListView.separated(
                separatorBuilder: (context, index) => Padding(
                    padding: EdgeInsets.only(left:10,right: 10),
                    child: Divider(color: Colors.grey)),
               // padding: EdgeInsets.all(6),
                itemCount: streamList.length,
                itemBuilder: (context, index) {
                  return Container(color: Colors.white, child: InkWell(
                      onTap: ()=>  navigateToChatPage(streamList[index]),
                      child: itemContent(index)));
                })),
      ],
    );
  }
/*
  Widget streamAds() {
    return Image.asset(IconConstants.IC_RHYTHM_ADS,
        width: SizeConfig.screenWidth, fit: BoxFit.fitWidth);
  }*/

  Widget itemContent(int index) {
    return Slidable(
      delegate: new SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
         Padding(
           padding: EdgeInsets.only(left: 20,right: 20,top: 10),
           child: Container(
             padding: EdgeInsets.only(left: 20,right: 20),
             width: 90.0,
             height: 90.0,
             decoration: new BoxDecoration(shape: BoxShape.circle,
               border: Border.all(color: Colors.blue,width: 2.0)),

             child: Image.asset("assets/images/person.png",width: 5,),),
         ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                    "${streamList[index].firstName}",
                    style: themeData.textTheme.title.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15)),
                SizedBox(height: 5),
                Text("${streamList[index].title}",
                    style: themeData.textTheme.subtitle.copyWith(
                        color: Colors.black54)),
                SizedBox(height: 10),
              ],
            ),
          ),
          Icon(Icons.arrow_forward_ios,color: Colors.black,),
          SizedBox(width: 20),
        ],
      ),
      actions: <Widget>[
        /*new IconSlideAction(
          caption: 'Archive',
          color: Colors.blue,
          icon: Icons.archive,
          onTap: () => {},
        ),
        new IconSlideAction(
          caption: 'Share',
          color: Colors.indigo,
          icon: Icons.share,
          onTap: () => {},
        ),*/
      ],
      secondaryActions: <Widget>[
        new IconSlideAction(
          caption: 'Archive',
          color: Colors.black,
          icon: Icons.archive,
          onTap: () => {},
        ),
        new IconSlideAction(
          caption: 'Delete',
          color: Colors.blue,
          icon: Icons.delete,
          onTap: () => _deleteData(index)
        ),
      ],
    );
  }

  void setStreamData() {
    InboxModel streamModel1 = InboxModel();
    streamModel1.isFriend = true;
    streamModel1.streamType = "1";
    streamModel1.profileImage = "https://i.pravatar.cc/150?img=1";
    streamModel1.coverImage =
    "https://images.unsplash.com/44/Y51aFguqRcGTgsYRYBXV_20140104_085932.jpg?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=100&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=200";
    streamModel1.firstName = "Richard Smith ";
    streamModel1.lastName = "Mcrae";
    streamModel1.email = "lenore.mcrae@gmail.com";
    streamModel1.mobile = "(000) 123-4567";
    streamModel1.dob = "05/25/1993";
    streamModel1.company = "Mcrae Inc";
    streamModel1.website = "www.lenore.com";
    streamModel1.title = "Consectetuer voluptatem similique";
    streamModel1.bio =
    "Hi! My name is Lenore and I’m born in Seattle. I am a traveling freelance photographer currently exploring the country. 5 miles away from you.";

    /*InboxModel streamModel2 = InboxModel();
    streamModel2.isFriend = true;
    streamModel2.streamType = "2";
    streamModel2.profileImage = "https://i.pravatar.cc/150?img=3";
    streamModel2.coverImage =
    "https://images.unsplash.com/photo-1509956072962-7ff0f36dd7ba?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=100&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=200";
    streamModel2.firstName = "Bryan";
    streamModel2.lastName = "Larock";
    streamModel2.email = "bryan.larock@gmail.com";
    streamModel2.mobile = "(000) 123-4567";
    streamModel2.dob = "05/25/1993";
    streamModel2.company = "Mcrae Inc";
    streamModel2.website = "www.bryan.com";
    streamModel2.title = "I saw you’re looking for a hiking buddy!r";
    streamModel2.bio =
    "Hi! My name is Bryan and I’m born in Seattle. I am a traveling freelance photographer currently exploring the country. 5 miles away from you.";

    InboxModel streamModel3 = InboxModel();
    streamModel3.isFriend = true;
    streamModel3.streamType = "1";
    streamModel3.profileImage = "https://i.pravatar.cc/150?img=8";
    streamModel3.coverImage =
    "https://images.unsplash.com/photo-1464115337528-7175b92ef5b4?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=100&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=200";
    streamModel3.firstName = "Gayle";
    streamModel3.lastName = "Weiser";
    streamModel3.email = "gayle.weiser@gmail.com";
    streamModel3.mobile = "(000) 123-4567";
    streamModel3.dob = "05/25/1993";
    streamModel3.company = "Weiser Inc";
    streamModel3.website = "www.weiser.com";
    streamModel3.title = "I’m selling my bike lol";
    streamModel3.bio =
    "Hi! My name is Gayle and I’m born in Seattle. I am a traveling freelance photographer currently exploring the country. 5 miles away from you.";

    InboxModel streamModel4 = InboxModel();
    streamModel4.isFriend = true;
    streamModel4.streamType = "2";
    streamModel4.profileImage = "https://i.pravatar.cc/150?img=4";
    streamModel4.coverImage =
    "https://images.unsplash.com/photo-1464115337528-7175b92ef5b4?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=100&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=200";
    streamModel4.firstName = "Adam";
    streamModel4.lastName = "Willy";
    streamModel4.email = "gayle.weiser@gmail.com";
    streamModel4.mobile = "(000) 123-4567";
    streamModel4.dob = "05/25/1993";
    streamModel4.company = "Adam Inc";
    streamModel4.website = "www.willy.com";
    streamModel4.title = "Hey girl!!";
    streamModel4.bio =
    "Hi! My name is Gayle and I’m born in Seattle. I am a traveling freelance photographer currently exploring the country. 5 miles away from you.";*/


    setState(() {
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
      streamList.add(streamModel1);
    });
  }

 void _deleteData(int index) {
    setState(() {
      streamList.remove(index);
    } );
 }
  void navigateToChatPage(InboxModel stream) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChatPage(streamModel: stream)));
  }


}