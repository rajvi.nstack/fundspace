import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/StreamModel.dart';
import 'package:fundspace/models/MessageModel.dart';

class ChatPage extends StatefulWidget {
  final InboxModel streamModel;

  ChatPage({Key key, @required this.streamModel}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState(this.streamModel);
}

class _ChatPageState extends State<ChatPage> {
  final InboxModel streamModel;
  List<MessageModel> messageList = List<MessageModel>();
  TextEditingController _messageTextController = new TextEditingController();

  _ChatPageState(this.streamModel);

  ThemeData themeData;
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setMessageData();
  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Scaffold(
        //backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
           iconTheme: IconThemeData(color: Colors.black),
            title: Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: ExactAssetImage('assets/images/user.png'),
                  minRadius: 23,
                  maxRadius: 23,
                  backgroundColor: Colors.white,
                ),
                 /*Container(
                    width: 42.0,
                    height: 42.0,
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                          image: null,
                            
                        )
                    )),*/
                SizedBox(width: 10,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(streamModel.firstName,style: TextStyle(color: Colors.black),),
                    Text(streamModel.firstName,style: TextStyle(color: Colors.black,fontSize: 10),),
                    //Text(messageList.firstName),
                  ],
                ),
              ],
            ),

        ),
        body: ProgressWidget(
            isShow: _isLoading,
            opacity: 0.6,
            child: SafeArea(child: chatBody())));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget chatBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // streamBanner(),
        Expanded(
            child: ListView.builder(
                reverse: true,
                shrinkWrap: true,
                padding: EdgeInsets.all(5),
                itemCount: messageList.length,
                itemBuilder: (context, index) {
                  if (messageList[index].isMine) {
                    return messageItemMine(index);
                  } else {
                    return messageItemOther(index);
                  }
                })),
        sendMessage()
      ],
    );
  }

  Widget messageItemMine(int index) {
    return Column(
crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Container(
          height: 100,
          width: 250,
          margin: EdgeInsets.only(left: 30, right: 20, top: 6, bottom: 10),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(0xFF90CAF9),
              borderRadius: BorderRadius.all(Radius.circular(18))),
          child: Center(
            child: Text(messageList[index].messageContent,
                style: TextStyle(color: Colors.black)),
          ),
        ),
       /* Text(messageList[index].time,
            style: TextStyle(color: Colors.white)),*/
      ],
    );
  }

  Widget messageItemOther(int index) {
    return Row(
      children: <Widget>[
      /*  Padding(
          padding: EdgeInsets.only(left: 5,right: 5,top: 10),
          child: Container(
            padding: EdgeInsets.only(left: 20,right: 20),
            width: 40.0,
            height: 40.0,
            decoration: new BoxDecoration(shape: BoxShape.circle,
                border: Border.all(color: Colors.blue,width: 2.0)),

            child: Image.asset("assets/images/person.png",width: 5,),),
        ),*/
        SizedBox(width: 10),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 80,
                width: 230,
                //color: Color(0xFFEEEEEE),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(

                    //border: Border.all(color: Colors.black87),
                    color: Color(0xFFEEEEEE),
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Center(
                  child: Text(messageList[index].messageContent,
                      style: TextStyle(color: Colors.black87)),
                ),
              ),
              /*Text(messageList[index].time,
                  style: TextStyle(color: Colors.white)),*/

            ],
          ),
        ),
        SizedBox(width: 15),
      ],
    );
  }

/*  Widget streamBanner() {
    return Image.asset(IconConstants.IC_CHAT_BANNER,
        width: SizeConfig.screenWidth, fit: BoxFit.cover);
  }*/

  Widget chatMessages() {
    return Container(child: Text("Messages"));
  }

  Widget sendMessage() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left:8.0),
          child: InkWell(
              onTap: () => sendChatMessage(_messageTextController.text),
              child: Icon(Icons.add)),
        ),
        Expanded(
          child: Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                  controller: _messageTextController,
                  style: TextStyle(color: Colors.black),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.send,
                  onChanged: (text) {},
                  onSubmitted: (value) {
                    sendChatMessage(value);
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      labelStyle: TextStyle(color: Colors.black),
                      hintStyle: TextStyle(color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide:
                              BorderSide(width: 1.0, color: Colors.black)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide:
                              BorderSide(width: 1.0, color: Colors.black)),
                      disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide:
                              BorderSide(width: 1.0, color: Colors.grey)),
                      hintText: "Type a message"))),
        ),
        InkWell(
            onTap: () => sendChatMessage(_messageTextController.text),
            child: Icon(Icons.send)),
        SizedBox(
          width: 15,
        )
      ],
    );
  }

  void setMessageData() {
    MessageModel messageModel1 = MessageModel();
    messageModel1.messageId = "1";
    messageModel1.messageContent = "Vivamus sed aliquet odio";
    messageModel1.time = "2H AGO";
    messageModel1.isMine = false;

    MessageModel messageModel2 = MessageModel();
    messageModel2.messageId = "2";
    messageModel2.messageContent = "In egestas mauris faucibus sit amet. Mauris sed.";
    messageModel1.time = "2H AGO";
    messageModel2.isMine = true;

    MessageModel messageModel3 = MessageModel();
    messageModel2.messageId = "2";
    messageModel2.messageContent = "Mauris ut tincidunt felis, non euismod magna. Nam eget justo tinclibero.";
    messageModel1.time = "2H AGO";
    messageModel2.isMine = false;

    MessageModel messageModel4 = MessageModel();
    messageModel2.messageId = "2";
    messageModel2.messageContent = "Nunc eget pulvinar ligula. Phasellus sed pharetra risus. Duis eu posuere nisl. accumsan, ut ultrice...";
    messageModel1.time = "2H AGO";
    messageModel2.isMine = true;

    setState(() {
      messageList.insert(0, messageModel1);
      messageList.insert(0, messageModel2);

   /*   messageList.insert(0, messageModel3);
      messageList.insert(0, messageModel4);*/
    });
  }

  void sendChatMessage(String value) {
    if (value.trim().isNotEmpty) {
      MessageModel messageModel1 = MessageModel();
      messageModel1.messageContent =value;
      messageModel1.time = "1H AGO";
      messageModel1.isMine = true;

      MessageModel messageModel2 = MessageModel();
      messageModel2.messageContent = "In egestas mauris faucibus sit amet. Mauris sed.";
      messageModel1.time = "1H AGO";
      messageModel2.isMine = false;
      _messageTextController.text = "";
      setState(() {
        messageList.insert(0, messageModel1);
        messageList.insert(0, messageModel2);
      });
    }
  }
}
