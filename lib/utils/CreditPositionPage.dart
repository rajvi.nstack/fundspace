import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class CreditPositionPage extends StatefulWidget {
  @override
  _MyCreditPositionPage createState() => _MyCreditPositionPage();
}

class _MyCreditPositionPage extends State<CreditPositionPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
    ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    loginHeader(),
                    SizedBox(height: 20),
                    loginContent(),
                    SizedBox(height: 15),
                    //separator(),
                    //socialLoginTabUI()
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 22),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 12),
            conLogin(),
            SizedBox(height: 10),
            centerData(),
            SizedBox(height: 10),
            conLogin(),
            SizedBox(height: 20),
            footerData(),
            SizedBox(
              height: 40,
            ),
            Container(
              width: double.infinity,
              height: 3,
              color: Color(0xFFEEEEEE),
            ),
            SizedBox(height: 55),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: loginButton(),
            ),
            SizedBox(height: 15),

            //newUser(),
          ],
        ),
      ),
    );
  }

  Widget conLogin() {
    return Container(
      width: double.infinity,
      height: 3,
      color: Colors.blue,
    );
  }

  Widget centerData() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Registered court\njudgements or CCJ",
              style: TextStyle(fontSize: 16, fontFamily: 'Open Sans'),
            ),
            Icon(
              Icons.keyboard_arrow_down,
              size: 45,
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Main\nBorrower/Director",
                style: TextStyle(fontSize: 16, fontFamily: 'Open Sans'),
              ),
              Text(
                "Second\nBorrower/Director",
                style: TextStyle(fontSize: 16, fontFamily: 'Open Sans'),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                child: Container(
                    //color:Colors.blue,
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.blue),
                    child: Center(
                        child: FlatButton(
                      child: Text(
                        "Yes",
                        style: TextStyle(color: Colors.white),
                      ), onPressed: () {},
                    )
                    )),
              ),
              Container(
                  //color:Colors.blue,
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Colors.blue,
                      width: 2,
                    ),
                  ),
                  child: Center(
                      child: FlatButton(
                    child: Text(
                      "No",
                      style: TextStyle(color: Colors.black),
                    ),
                    onPressed: () {},
                  ))),
              Container(
                  //color:Colors.blue,
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Colors.blue,
                      width: 2,
                    ),
                  ),
                  child: Center(
                      child:  FlatButton(
                        child: Text(
                          "Yes",
                          style: TextStyle(color: Colors.black),
                        ), onPressed: () {},
                      )
                  )),
              Container(
                  //color:Colors.blue,
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Colors.blue,
                      width: 2,
                    ),
                  ),
                  child: Center(
                      child:  FlatButton(
                        child: Text(
                          "No",
                          style: TextStyle(color: Colors.black),
                        ), onPressed: () {},
                      ))),
            ],
          ),
        )
      ],
    );
  }

/*  RaisedButton(
  color: Colors.blue,
  onPressed: null,child: Text("Yes",style: TextStyle(color: Colors.white),),),*/
  Widget footerData() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Officer of a company in\nwhich a receiver or liquidator\nwas appointed",
              style: TextStyle(fontSize: 16, fontFamily: 'Open Sans'),
            ),
            Icon(
              Icons.keyboard_arrow_up,
              size: 45,
            )
          ],
        ),
      ],
    );
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          "Complete this later",
          style: TextStyle(color: Colors.blue, fontSize: 20),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: SizedBox(
            height: 50,
            width: 150,
            child: FlatButton(
              color: Colors.blue,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(7.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "NEXT",
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                  )
                ],
              ),
              onPressed: () {
                // setLoading(true);
                _validateInputs();
              },
            ),
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, bottom: 25, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () {
                return WillPopScope(onWillPop: () async {
                  return false;
                });
              },
              //onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Credit Position",
            style: TextStyle(fontSize: 28, fontFamily: 'Open Sans'),
          ),
          SizedBox(height: 20),
          Text(
            "The information below refers to the personal",
            style: TextStyle(fontSize: 14, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "borrowers/company directors.",
            style: TextStyle(fontSize: 14, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
           //crossAxisAlignment: CrossAxisAlignment.s,

            children: <Widget>[
              Text(
                "Bankruptcies or individual\nvoluntary arrangements",
                style: TextStyle(fontSize: 18, fontFamily: 'Open Sans'),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35.0),
                child: Icon(
                  Icons.keyboard_arrow_up,
                  size: 45,
                ),
              ),
              //Icon(Icons.keyboard_arrow_up),
            ],
          ),
        ],
      ),
    );
  }
}
