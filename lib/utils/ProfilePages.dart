
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/utils/AboutYouPage.dart';
import 'package:fundspace/utils/AssetLiabilitiesPages.dart';
import 'package:fundspace/utils/CompanyDetailsPage.dart';
import 'package:fundspace/utils/CreditPositionPage.dart';
import 'package:fundspace/utils/ProjectScreen.dart';
class ProfilePages extends StatefulWidget {
  ProfilePages({Key key}) : super(key: key);

  @override
  _MyProfilePages createState() => _MyProfilePages();
}

class _MyProfilePages extends State<ProfilePages>  with SingleTickerProviderStateMixin{
  ThemeData themeData;
  bool _isLoading = false;
  Animation<double> _progressAnimation;
  AnimationController _progressAnimcontroller;
  final controller=PageController();
  @override
  void initState() {
    super.initState();

    _progressAnimcontroller = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
    );

    _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
        .animate(_progressAnimcontroller);

    _setProgressAnim(0, 1);
  }

  double growStepWidth, beginWidth, endWidth = 0.0;
  int totalPages = 6;

  _setProgressAnim(double maxWidth, int curPageIndex) {
    setState(() {
      growStepWidth = maxWidth / totalPages;
      beginWidth = growStepWidth * (curPageIndex - 1);
      endWidth = growStepWidth * curPageIndex;

      _progressAnimation = Tween<double>(begin: beginWidth, end: endWidth)
          .animate(_progressAnimcontroller);
    });

    _progressAnimcontroller.forward();
  }
  //int step=1;


  @override
  Widget build(BuildContext context) {
    var mediaQD = MediaQuery.of(context);
    var maxWidth = mediaQD.size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: InkWell(
            onTap: ()=>Navigator.of(context).pop(),
            child: Icon(Icons.keyboard_arrow_left,color: Colors.black,)),
        title: Center(
          child: Text(
            "Profile",
            style: TextStyle(color: Colors.black),
          ),
        ),

      ),
      body:Column(
        children: <Widget>[

          Container(
            color: Colors.transparent,
            child: Row(
              children: <Widget>[
                AnimatedProgressBar(
                  animation: _progressAnimation,
                ),
                Expanded(
                  child: Container(
                    height: 2.0,
                    width: double.infinity,
                    decoration: BoxDecoration(color: Color(0xFFEEEEEE)),
                  ),
                )
              ],
            ),
          ),
           Flexible(
            /* padding: EdgeInsets.only(top:20),
            height: double.infinity,*/
            child: PageView(
              controller:controller ,
              scrollDirection: Axis.horizontal,
              onPageChanged: (i) {
                //index i starts from 0!
                setState(() {
                  _progressAnimcontroller.reset(); //reset the animation first
                  _setProgressAnim(maxWidth, i + 1);
                });

              },

              children: <Widget>[
                //aboutYouPage(),
                AboutYouPage(),
                CompanyDetailsPage(),
                CreditPositionPage(),
                AssetLiabilitiesPages(),
                ProjectScreen()
              ],
            ),
          ),
        ],
      )
    );
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }
}

class AnimatedProgressBar extends AnimatedWidget {
  AnimatedProgressBar({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Container(
      height: 4.0,
      width: animation.value,
      decoration: BoxDecoration(color: Colors.blue),
    );
  }
}


//https://www.stacksecrets.com/flutter/adding-a-progress-bar-animation-in-page-view
