import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/HomeScreen.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/models/AssetLiabilities.dart';
import 'package:fundspace/models/PreviousProject.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class ProjectScreen extends StatefulWidget {
  ProjectScreen({Key key}) : super(key: key);

  @override
  _ProjectScreenState createState() => _ProjectScreenState();
}

class _ProjectScreenState extends State<ProjectScreen> {
  List<PreviousProject> projectList = [];
  bool _isLoading = false;
  ThemeData themeData;
  PreviousProject previousProject;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ScrollController _rrectController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    onAddForm();

    //setStreamData();
  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: streamBody()),
    ));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget streamBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  loginHeader(),
                  SizedBox(height: 20),
                  showList(),
                  SizedBox(height: 15),
                  addProject(),
                  SizedBox(height: 15),
                  loginButton(),
                  SizedBox(height: 15),
                  //separator(),
                  //socialLoginTabUI()
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 38, top: 30, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "Previous Projects",
            style: TextStyle(fontSize: 28, fontFamily: 'Open Sans'),
          ),
          SizedBox(height: 20),
          Text(
            "Lenders prefer developers with previous",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "project experience so if you have any please",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "add below if not you can skip this section.e",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),
          ),
        ],
      ),
    );
  }

  Widget showList() {
    return Container(
      // height:400 ,
      //key: _formKey,
      child: DraggableScrollbar.rrect(
        controller: _rrectController,
        backgroundColor: Colors.blue,

        child: ListView.builder(
            controller: _rrectController,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: projectList.length,
            itemBuilder: (_, i) => ProjectForm(
                  previousProject: projectList[i],
                )),
      ),
    );
  }

  void onAddForm() {
    setState(() {
      projectList.add(PreviousProject());
    });
  }

  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Complete this later",
                style: TextStyle(fontSize: 18, color: Colors.blue),
              ),
              SizedBox(
                width: 10,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 48,
          width: 140,
          child: Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: FlatButton(
              color: Colors.blue,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Finish",
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                  )
                ],
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
                // setLoading(true);
                // _validateInputs();
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget addProject() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: InkWell(
        onTap: () {
          print("Demo");
          //multipleProject();
          onAddForm();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              Icons.add,
              size: 30,
              color: Colors.blue,
            ),
            Text(
              "Add Project",
              style: TextStyle(fontSize: 20, color: Colors.blue),
            ),
          ],
        ),
      ),
    );
  }
}

class ProjectForm extends StatefulWidget {
  final PreviousProject previousProject;
  final state = _ProjectFormState();

  ProjectForm({Key key, this.previousProject}) : super(key: key);

  @override
  _ProjectFormState createState() => state;
}

class _ProjectFormState extends State<ProjectForm> {
//  final form = GlobalKey<FormState>();

  final FocusNode _addressFocus = FocusNode();
  final FocusNode _purchasePriceFocus = FocusNode();
  final FocusNode _buildCostsPriceFocus = FocusNode();
  final FocusNode _salePriceFocus = FocusNode();
  final FocusNode _dateCompletedFocus = FocusNode();
  final FocusNode _profileFocus = FocusNode();
  final FocusNode _lenderFocus = FocusNode();
  final _formKey = GlobalKey<FormState>();
  TextEditingController addressController = new TextEditingController();
  TextEditingController purchasePriceController = new TextEditingController();
  TextEditingController buildCostsController = new TextEditingController();
  TextEditingController salePriceController = new TextEditingController();
  TextEditingController dateCompletedController = new TextEditingController();
  TextEditingController profileController = new TextEditingController();
  TextEditingController lenderController = new TextEditingController();

  String _address;
  String _purchasePrice;
  String _buildCosts;
  String _salePrice;
  String _dateCompletede;
  String _profile;
  String _lender;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Material(
        elevation: 1,
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(8),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[itemContent()],
          ),
        ),
      ),
    );
  }

  Widget itemContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 12),
              Text(
                "Project1",
                style: TextStyle(color: Colors.black, fontSize: 18),
              ),
              SizedBox(height: 5),
              address(),
              SizedBox(height: 2),
              purchasePrice(),
              SizedBox(height: 2),
              buildCosts(),
              SizedBox(height: 2),
              salePrice(),
              SizedBox(height: 2),
              dateCompleted(),
              SizedBox(height: 2),
              profile(),
              SizedBox(height: 2),
              lender(),
              SizedBox(height: 2),
              //multipleProject()

              //newUser(),
            ],
          ),
        ),
      ),
    );
  }

  Widget address() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: addressController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _addressFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _addressFocus, _purchasePriceFocus);
        },
        onSaved: (String val) {
          _address = val;
        },
        decoration: InputDecoration(
          hintText: "Ahmedabad",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Address:",
        ));
  }

  Widget purchasePrice() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: purchasePriceController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _purchasePriceFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(
              context, _purchasePriceFocus, _buildCostsPriceFocus);
        },
        onSaved: (String val) {
          _purchasePrice = val;
        },
        decoration: InputDecoration(
          hintText: "20000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Purchase Price:",
        ));
  }

  Widget buildCosts() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: buildCostsController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _buildCostsPriceFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _buildCostsPriceFocus, _salePriceFocus);
        },
        onSaved: (String val) {
          _buildCosts = val;
        },
        decoration: InputDecoration(
          hintText: "20000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "PBuild costs:",
        ));
  }

  Widget salePrice() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: salePriceController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _salePriceFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _salePriceFocus, _dateCompletedFocus);
        },
        onSaved: (String val) {
          _salePrice = val;
        },
        decoration: InputDecoration(
          hintText: "20000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "GDV/Sale price:",
        ));
  }

  Widget dateCompleted() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: dateCompletedController,
        validator: (String value) {
          return InputValidator.validateDateOfBirth(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _dateCompletedFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _dateCompletedFocus, _profileFocus);
        },
        onSaved: (String val) {
          _dateCompletede = val;
        },
        decoration: InputDecoration(
          hintText: "20/4/2019",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Date completed:",
        ));
  }

  Widget profile() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: profileController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _profileFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _profileFocus, _lenderFocus);
        },
        onSaved: (String val) {
          _profile = val;
        },
        decoration: InputDecoration(
          hintText: "20000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Profile:",
        ));
  }

  Widget lender() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: lenderController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _lenderFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _lenderFocus, _lenderFocus);
        },
        onSaved: (String val) {
          _lender = val;
        },
        decoration: InputDecoration(
          hintText: "20000",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Lender:",
        ));
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
