import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';

class AboutYouPage extends StatefulWidget {
  //AnimationController _progressAnimcontroller;


  //AboutYouPage(this._progressAnimcontroller);

  @override
  _MyAboutYouPage createState() => _MyAboutYouPage();
}

class _MyAboutYouPage extends State<AboutYouPage> {
  final FocusNode _developerEx = FocusNode();
  final FocusNode _dateFocus = FocusNode();
  final FocusNode _occupationFocus = FocusNode();
  final FocusNode _employmentFocus = FocusNode();
  final FocusNode _incomeFocus = FocusNode();
  final FocusNode _nationalityFocus = FocusNode();
  final FocusNode _residentialFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController developerController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController occupationController = new TextEditingController();
  TextEditingController employmentController = new TextEditingController();
  TextEditingController incomeController = new TextEditingController();
  TextEditingController nationalityController = new TextEditingController();
  TextEditingController residentialController = new TextEditingController();
  ThemeData themeData;
  bool _isLoading = false;
  bool _autoValidate = false;
  String _developer;
  String _dateOfBirth;
  String _occupation;
  String _employment;
  String _income;
  String _nationality;
  String _residential;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
    ));
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    loginHeader(),
                    SizedBox(height: 20),
                    loginContent(),
                    SizedBox(height: 15),
                    //separator(),
                    //socialLoginTabUI()
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all( Radius.circular(10.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 12),
              Text(
                "Developer",
                style: TextStyle(color: Colors.black, fontSize: 18),
              ),
              SizedBox(height: 7),
              developerExperince(),
              SizedBox(height: 10),
              dateOfBirth(),
              SizedBox(height: 10),
              occupation(),
              SizedBox(height: 10),
              employmentStatus(),
              SizedBox(height: 10),
              annualIncome(),
              SizedBox(height: 10),
              nationality(),
              SizedBox(height: 10),
              residentialStatus(),
              SizedBox(height: 55),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: loginButton(),
              ),
              SizedBox(height: 15),

              //newUser(),
            ],
          ),
        ),
      ),
    );
  }

  Widget developerExperince() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: developerController,
        validator: (String value) {
          return InputValidator.validateDeveloper(value);
        },
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        focusNode: _developerEx,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _developerEx, _dateFocus);
        },
        onSaved: (String val) {
          _developer = val;
        },
        decoration: InputDecoration(
          hintText: "5 years",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Developer Experiance:",
        ));
  }
  Widget dateOfBirth() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: dateController,
        validator: (String value) {
          return InputValidator.validateDateOfBirth(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        readOnly: true,
        onTap: () => _showDatePicker(),
        focusNode: _dateFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _dateFocus, _occupationFocus);
        },
        onSaved: (String val) {
          _dateOfBirth = val;
        },
        decoration: InputDecoration(
          hintText: "5 years",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Date of Birth:",
        ));
  }
  void _showDatePicker() {
    String _format = 'yyyy-MMMM-dd';
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.blue)),
        cancel: Text('cancel', style: TextStyle(color: Colors.red)),
      ),
      initialDateTime: DateTime.now(),
      dateFormat: _format,
      onConfirm: (dateTime, List<int> index) {
        print('$dateTime');
        setState(() {
          _dateOfBirth = "${dateTime.year}-${dateTime.month}-${dateTime.day}";
          dateController.text = _dateOfBirth;
        });
      },
    );
  }
  Widget occupation() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: occupationController,
        validator: (String value) {
          return InputValidator.validateOccupation(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _occupationFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _occupationFocus, _employmentFocus);
        },
        onSaved: (String val) {
          _occupation = val;
        },
        decoration: InputDecoration(
          hintText: "Property Developer",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Occupation:",
        ));
  }
  Widget employmentStatus() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: employmentController,
        validator: (String value) {
          return InputValidator.validateEmploymentStatus(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _employmentFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _employmentFocus, _incomeFocus);
        },
        onSaved: (String val) {
          _employment = val;
        },
        decoration: InputDecoration(
          hintText: "Employed",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Employment Status:",
        ));
  }
  Widget annualIncome() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: incomeController,
        validator: (String value) {
          return InputValidator.validateAnnualIncome(value);
        },
        keyboardType: TextInputType.number,

        textInputAction: TextInputAction.next,
        focusNode: _incomeFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _incomeFocus, _nationalityFocus);
        },
        onSaved: (String val) {
          _income = val;
        },
        decoration: InputDecoration(
          hintText: "50k",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Approximate Annual Income:",
        ));
  }
  Widget nationality() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: nationalityController,
        validator: (String value) {
          return InputValidator.validateNationality(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        focusNode: _nationalityFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _nationalityFocus, _residentialFocus);
        },
        onSaved: (String val) {
          _nationality = val;
        },
        decoration: InputDecoration(
          hintText: "Indian",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Nationality:",
        ));
  }
  Widget residentialStatus() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: residentialController,
        validator: (String value) {
          return InputValidator.validateResidential(value);
        },
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        focusNode: _residentialFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _residentialFocus, _residentialFocus);
        },
        onSaved: (String val) {
          _residential = val;
        },
        decoration: InputDecoration(
          hintText: "Ahmedabad",
          hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 15),
          labelStyle: TextStyle(
              fontFamily: "Open Sans", fontSize: 16, color: Colors.black),
          labelText: "Current Residential Status:",
        ));
  }



  Widget loginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          height: 50,
          width: 150,
          child: FlatButton(
            color: Colors.blue,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(7.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "NEXT",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                )
              ],
            ),
            onPressed: () {
              // setLoading(true);
              _validateInputs();
            },
          ),
        ),
      ],
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_developer");
      //print("LOGIN password : $_loaction");
      //navigateToProfilePage();
      // userLogin();

    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, bottom: 25, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () {
                return WillPopScope(onWillPop: () async {
                  return false;
                });
              },
              //onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 50, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 20,
          ),
          Text(
            "About you",
            style: TextStyle(fontSize: 28, fontFamily: 'Open Sans'),
          ),
          SizedBox(height: 20),
          Text(
            "Once you’ve entered your basic profile information",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),

          ),
        SizedBox(height: 3,),
        Text(
        "below, you’ll be able to add more information about you",
        style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),),
          SizedBox(height: 3,),
          Text(
            "as a developer later. The more information you provide",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),),
          SizedBox(height: 3,),
          Text(
            "the more likely you are to receive the most ompetitive",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),),
          SizedBox(height: 3,),
          Text(
            "funding terms.",
            style: TextStyle(fontSize: 13, fontFamily: 'Open Sans'),),


        ],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
