import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fundspace/HomeScreen.dart';
import 'package:fundspace/custom_widget/progress_view.dart';
import 'package:fundspace/utils/SignUpScreen.dart';
import 'package:fundspace/utils/SplashScreen.dart';
import 'package:fundspace/utils/common_utils/input_validator.dart';
import 'package:fundspace/utils/common_utils/size_config.dart';
import 'package:fundspace/utils/constants/icon_contants.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _MyLoginScreen createState() => _MyLoginScreen();
}

class _MyLoginScreen extends State<LoginScreen> {
  ThemeData themeData;
  bool _isLoading = false;
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _autoValidate = false;
  bool passwordVisible=true;
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child:
          ProgressWidget(isShow: _isLoading, opacity: 0.6, child: loginBody()),
    ));
  }

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  Widget loginBody() {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    loginNavigation(),
                    loginHeader(),
                    loginContent(),
                    SizedBox(height: 15),
                    separator(),
                    socialLoginTabUI()
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget loginContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 12),
          emailInput(),
          SizedBox(height: 10),
          passwordInput(),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createAccount(),
              forgotPassword(),
            ],
          ),
          SizedBox(height: 55),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: loginButton(),
          ),
          SizedBox(height: 15),

          //newUser(),
        ],
      ),
    );
  }

  Widget emailInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: emailController,
        validator: (String value) {
          return InputValidator.validateEmail(value);
        },
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        focusNode: _emailFocus,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _emailFocus, _passwordFocus);
        },
        onSaved: (String val) {
          _email = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your email",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Email",
            suffixIcon: Icon(Icons.done)));
  }

  Widget passwordInput() {
    return TextFormField(
        style: TextStyle(color: Colors.black),
        controller: passwordController,
        validator: (String value) {
          return InputValidator.validatePassword(value);
        },
        keyboardType: TextInputType.visiblePassword,
        textInputAction: TextInputAction.done,
        focusNode: _passwordFocus,
        obscureText: passwordVisible,
        onFieldSubmitted: (term) {
          _validateInputs();
        },
        onSaved: (String val) {
          _password = val;
        },
        decoration: InputDecoration(
            hintText: "Enter your password",
            hintStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelStyle: TextStyle(fontFamily: "Open Sans", fontSize: 18),
            labelText: "Password",
          suffixIcon:GestureDetector(
            dragStartBehavior: DragStartBehavior.down,
            onTap: () {
              setState(() {
                passwordVisible = !passwordVisible;
              });
            },
            child: Icon(
              passwordVisible ? Icons.visibility_off : Icons.visibility,
              //semanticLabel: _obscureText ? 'show password' : 'hide password',
            ),
          ),
        ));
  }

  Widget forgotPassword() {
    return GestureDetector(
      onTap: () => navigateToForgotPasswordPage(),
      child: Text(
        "Forgot Password?",
        textAlign: TextAlign.right,
        style: TextStyle(color: Colors.blue, fontSize: 16),
        // style: themeData.textTheme.subtitle,
      ),
    );
  }

  Widget createAccount() {
    return GestureDetector(
      onTap: () => navigateToCreateAccount(),
      child: Text(
        "Create new account",
        textAlign: TextAlign.right,
        style: TextStyle(color: Colors.blue, fontSize: 16),
        // style: themeData.textTheme.subtitle,
      ),
    );
  }

  Widget loginButton() {
    return SizedBox(
      height: 45,
      child: FlatButton(
        color: Colors.blue,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(7.0)),
        child: Text(
          "Login",
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () {
          // setLoading(true);
          _validateInputs();
        },
      ),
    );
  }

  Widget separator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        //SizedBox(width: 10),
        Text("or login with",
            style: TextStyle(color: Colors.black, fontSize: 14),
            textAlign: TextAlign.center),
      ],
    );
  }

  Widget socialLoginTabUI() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          googleLogin(),
          SizedBox(width: 15),
          facebookLogin(),
        ],
      ),
    );
  }

  Widget facebookLogin() {
    return Image.asset(IconConstants.IC_facebook,
        width: 35, height: 35, color: Colors.black);
  }

  Widget googleLogin() {
    return Image.asset(IconConstants.IC_Google,
        width: 35, height: 35, color: Colors.black);
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      print("LOGIN email : $_email");
      print("LOGIN password : $_password");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
      //navigateToHomePage();
      // userLogin();
    } else {
      //If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget loginNavigation() {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 13, bottom: 25, right: 90),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: (){
                return WillPopScope(
                    onWillPop: () async {
                  return false;
                });
              },
              //onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back, size: 25, color: Colors.blue))
        ],
      ),
    );
  }

  Widget loginHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        appLogo(),
        SizedBox(height: 10),
      ],
    );
  }

  Widget appLogo() {
    return Image.asset(
      IconConstants.IC_APP_LOGO,
      width: 200,
      height: 200,
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void navigateToForgotPasswordPage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SplashScreen()));
  }

  void navigateToCreateAccount() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SignUpScreen()));
  }
}
