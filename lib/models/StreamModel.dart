import 'dart:convert';

List<InboxModel> streamModelFromJson(String str) => List<InboxModel>.from(
    json.decode(str).map((x) => InboxModel.fromJson(x)));

String streamModelToJson(List<InboxModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class InboxModel {
  String streamType = "1";
  String profileImage = "";
  String coverImage = "";
  String firstName = "";
  String lastName = "";
  String email = "";
  String mobile = "";
  String dob = "";
  String company = "";
  String website = "";
  String title = "";
  String facebookUrl = "";
  String instagramUrl = "";
  String twitterUrl = "";
  String linkedinUrl = "";
  String bio = "";
  bool isFriend = true;

  InboxModel({
    this.streamType,
    this.profileImage,
    this.coverImage,
    this.firstName,
    this.lastName,
    this.email,
    this.mobile,
    this.dob,
    this.company,
    this.website,
    this.title,
    this.facebookUrl,
    this.instagramUrl,
    this.twitterUrl,
    this.linkedinUrl,
    this.bio,
    this.isFriend,
  });

  factory InboxModel.fromJson(Map<String, dynamic> json) => InboxModel(
        streamType: json["streamType"],
        profileImage: json["profile_image"],
        coverImage: json["cover_image"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        mobile: json["mobile"],
        dob: json["dob"],
        company: json["company"],
        website: json["website"],
        title: json["title"],
        facebookUrl: json["facebook_url"],
        instagramUrl: json["instagram_url"],
        twitterUrl: json["twitter_url"],
        linkedinUrl: json["linkedin_url"],
        bio: json["bio"],
        isFriend: json["is_friend"],
      );

  Map<String, dynamic> toJson() => {
        "streamType": streamType,
        "profile_image": profileImage,
        "cover_image": coverImage,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "mobile": mobile,
        "dob": dob,
        "company": company,
        "website": website,
        "title": title,
        "facebook_url": facebookUrl,
        "instagram_url": instagramUrl,
        "twitter_url": twitterUrl,
        "linkedin_url": linkedinUrl,
        "bio": bio,
        "is_friend": isFriend,
      };
}
