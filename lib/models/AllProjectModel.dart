
class AllProjectModel {
  String projectName = "1";
  String projectDec = "1";
  String projectAmount = "";
  String fundingRequired = "";
  String debt = "";
  String xValue = "";


  AllProjectModel({
    this.projectName,
    this.projectDec,
    this.projectAmount,
    this.fundingRequired,
    this.debt,
    this.xValue,

  });

  factory AllProjectModel.fromJson(Map<String, dynamic> json) => AllProjectModel(
    projectName: json["projectName"],
    projectDec: json["projectDec"],
    projectAmount: json["projectAmount"],
    fundingRequired: json["fundingRequired"],
    debt: json["debt"],
    xValue: json["xValue"],

  );

  Map<String, dynamic> toJson() => {
    "projectName": projectName,
    "projectDec": projectDec,
    "projectAmount": projectAmount,
    "fundingRequired": fundingRequired,
    "debt": debt,
    "xValue": xValue,

  };
}