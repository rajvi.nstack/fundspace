class AssetLiabilities {
  String assetsName = "1";
  String totalValue = "";
  String outStandingValue = "";


  AssetLiabilities({
    this.assetsName,
    this.totalValue,
    this.outStandingValue,

  });

  factory AssetLiabilities.fromJson(Map<String, dynamic> json) => AssetLiabilities(
    assetsName: json["assetsName"],
    totalValue: json["totalValue"],
    outStandingValue: json["outStandingValue"],

  );

  Map<String, dynamic> toJson() => {
    "streamType": assetsName,
    "profile_image": totalValue,
    "cover_image": outStandingValue,

  };
}