// To parse this JSON data, do
//
//     final messageModel = messageModelFromJson(jsonString);

import 'dart:convert';

List<MessageModel> messageModelFromJson(String str) => List<MessageModel>.from(json.decode(str).map((x) => MessageModel.fromJson(x)));

String messageModelToJson(List<MessageModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MessageModel {
  String messageId;
  String messageContent;
  bool isMine;
  String time;

  MessageModel({
    this.messageId,
    this.messageContent,
    this.isMine,
    this.time
  });

  factory MessageModel.fromJson(Map<String, dynamic> json) => MessageModel(
    messageId: json["message_id"],
    messageContent: json["message_content"],
    isMine: json["isMine"],
    time: json["time"]
  );

  Map<String, dynamic> toJson() => {
    "message_id": messageId,
    "message_content": messageContent,
    "isMine": isMine,
    "time": time,
  };
}
