
class PreviousProject {
  String project="";
  String address = "1";
  String purchasePrice = "";
  String buildCosts = "";
  String salePrice = "";
  String dateCompleted = "";
  String profit = "";
  String lender = "";


  PreviousProject({this.project,this.address, this.purchasePrice, this.buildCosts,
      this.salePrice, this.dateCompleted, this.profit, this.lender});

  factory PreviousProject.fromJson(Map<String, dynamic> json) => PreviousProject(
    project: json["project"],
    address: json["address"],
    purchasePrice: json["purchasePrice"],
    buildCosts: json["buildCosts"],
    salePrice: json["salePrice"],
    dateCompleted: json["dateCompleted"],
    profit: json["profit"],
    lender: json["lender"],

  );

  Map<String, dynamic> toJson() => {
    "project":project,
    "address": address,
    "purchasePrice": purchasePrice,
    "buildCosts": buildCosts,
    "salePrice": salePrice,
    "dateCompleted": dateCompleted,
    "profit": profit,
    "lender": lender,


  };
}